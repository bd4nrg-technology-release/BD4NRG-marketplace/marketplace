import logging

class Logger:
    def __init__(self, name):
        self.logger = logging.getLogger(name)
        self.logger.setLevel(logging.DEBUG)

        # create a file handler
        handler = logging.FileHandler('volume/api.log')
        handler.setLevel(logging.DEBUG)

        # create a logging format
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)

        # add the handlers to the logger
        self.logger.addHandler(handler)

    def debug(self, message):
        self.logger.debug(message)

    def info(self, message):
        self.logger.info(message)

    def warning(self, message):
        self.logger.warning(message)

    def error(self, message):
        self.logger.error(message)

class UrlGenerator:
    def __init__(self, base_url, base_port="", protocol='http://'):
        self.base_url = protocol + base_url + ':' + base_port

    def generate_endpoint_url(self, endpoint_name):
        return f"{self.base_url}{endpoint_name}"
    
