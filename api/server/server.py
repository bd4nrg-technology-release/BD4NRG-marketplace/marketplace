import os
import re
import time
import requests
import base64
import tempfile
import redis
import json
from datetime import datetime
from io import BytesIO
from dotenv import load_dotenv
from functools import wraps
from minio import Minio, error
from urllib.parse import urlencode

from flask import Flask
from flask import request, jsonify, send_file, g, stream_with_context, Response
from flask import make_response
from flask_cors import CORS

from server_utils import Logger, UrlGenerator

#Config
app = Flask(__name__)
# app.debug = True
CORS(app, resources={r"/*": {"origins": "*", "allow_headers": ["Content-Type", "Authorization"]}})

logger = Logger("api")

load_dotenv()

minio_url = UrlGenerator(os.getenv('MINIO_HOST'), os.getenv('MINIO_PORT'))
eais_url = UrlGenerator(os.getenv('EAIS_HOST'), os.getenv('EAIS_PORT'))
ads_url = UrlGenerator(os.getenv('ADS_HOST'), os.getenv('ADS_PORT'))
augs_url = UrlGenerator(os.getenv('AUGS_HOST'), os.getenv('AUGS_PORT'))
idm_url = UrlGenerator(os.getenv('IDM_HOST'), os.getenv('IDM_PORT'))
at_url = UrlGenerator(os.getenv('AT_HOST'), protocol='https://')
client_id = os.getenv('CLIENT_ID')
secret_id = os.getenv('SECRET_ID')
minio_root_user = os.getenv('MINIO_ROOT_USER')
minio_root_password = os.getenv('MINIO_ROOT_PASSWORD')
redis_terms_client = redis.StrictRedis(host='redis', port=6379, db=0, decode_responses=True)
redis_jobs_client = redis.StrictRedis(host='redis', port=6379, db=1, decode_responses=True)
redis_historical_client = redis.StrictRedis(host='redis', port=6379, db=2, decode_responses=True)

# XXX: BLOCKCHAIN VBLES
tp_url = UrlGenerator(os.getenv('TP_HOST'), os.getenv('TP_PORT'))



logger.info("******\tStarting API\t******")
logger.info("Minio url: " + minio_url.base_url)
logger.info("Eais url: " + eais_url.base_url)
logger.info("Ads url: " + ads_url.base_url)
logger.info("Augs url: " + augs_url.base_url)
logger.info("TP url: " + tp_url.base_url)

# Utils

# def add_redis_job(username, job_id):
#     redis_jobs_client.sadd(username, job_id)

def add_redis_job_augs(username, job_id):
    # logger.info("Starting add_redis_job_augs")
    redis_jobs_client.sadd(username, "AUGS_" + job_id)

def add_redis_job_eais(username, job_id):
    # logger.info("Starting add_redis_job_eais")
    redis_jobs_client.sadd(username, "EAIS_" + job_id)

def add_redis_job_ads(username, job_id):
    # logger.info("Starting add_redis_job_ads")
    redis_jobs_client.sadd(username, "ADS_" + job_id)


# def get_redis_jobs(username): # https://www.ionos.es/digitalguide/hosting/cuestiones-tecnicas/redis-tutorial-paso-a-paso/
#     return redis_jobs_client.smembers(username)

def check_job_by_condition_ads(element):
    # logger.info("Starting check_job_by_condition_ads")
    if 'ADS_' in element:
        return True
    return False

def check_job_by_condition_eais(element):
    # logger.info("Starting check_job_by_condition_eais")
    if 'EAIS_' in element:
        return True
    return False

def get_redis_jobs_ads(username): # https://favtutor.com/blogs/filter-list-python
    logger.info("Starting get_redis_jobs_ads")
    initial_list = redis_jobs_client.smembers(username) 
    logger.info(f"get_redis_jobs_ads , initial_list:{initial_list}")
    return list(filter(check_job_by_condition_ads, initial_list))

def get_redis_jobs_eais(username): # https://favtutor.com/blogs/filter-list-python
    logger.info("Starting get_redis_jobs_eais")
    initial_list = redis_jobs_client.smembers(username)
    logger.info(f"get_redis_jobs_eais , initial_list:{initial_list}")
    return list(filter(check_job_by_condition_eais, initial_list))


def _extract_job_id_from_cycle(cycle: str) -> str:
    # cycles_2024-01-25_14:30:33_1d5d139f-ed24-4db3-9be1-120242d98c72.json
    result = re.sub(r'cycles_\d{4}-\d{2}-\d{2}_\d{2}:\d{2}:\d{2}_', '', cycle)
    result = re.sub(r'.json', '', result)
    return result

def _extract_job_id_from_redis_jobs_ads_logs(redis_jobs_log: str) -> str:
    # ADS_f9c118bb-f252-454d-897f-6a941d6cfa55
    result = re.sub(r'ADS_', '', redis_jobs_log)
    return result

def _extract_job_id_from_redis_jobs_eais_logs(redis_jobs_log: str) -> str:
    
    result = re.sub(r'EAIS_', '', redis_jobs_log)
    return result

def _extract_job_id_from_redis_jobs_augs_logs(redis_jobs_log: str) -> str:
    
    result = re.sub(r'AUGS_', '', redis_jobs_log)
    return result


@app.route('/api/login', methods=['POST'])
def login():
    try:
        data = request.json
        username = data['username']
        password = data['password']

        # Encode client_id and secret_id in base64
        credentials = f"{client_id}:{secret_id}"
        encoded_credentials = base64.b64encode(credentials.encode('utf-8')).decode('utf-8')
        auth_header = f"Basic {encoded_credentials}"

        logger.info(f"Authentication for user {username} and password {password} with auth header: \n {auth_header}")
        body = {
            'username': username,
            'password': password,
            'grant_type': 'password',
        }

        res = requests.post(
            idm_url.generate_endpoint_url('/oauth2/token'),
            headers={
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': auth_header,
            },
            data=urlencode(body)
        )
        logger.info(f"Response code for request to endpoint {idm_url.generate_endpoint_url('/oauth2/token')} is \n {res.json()} \n with status code: {res.status_code}")
        if res.status_code == 200:
            user_accepted = redis_terms_client.get(username)
            logger.info(f"Response was 200 and user_accepted value: {user_accepted}")
            if user_accepted:
                logger.info(f"User has already accepted terms")
                return jsonify(res.json()), 200
            else:
                logger.info(f"User has not accepted terms")
                return jsonify({'error': 'Forbidden, user hasn\'t accepted regulations'}), 403
        else:
            logger.info(f"User has not accepted terms")
            return jsonify({'error': res.text}), res.status_code
    except Exception as e:
        logger.error("Error login")
        logger.error(f"Exception: {str(e.message)}")  # Log the exception itself, to get more detailed info

def extract_user(auth_token):
    logger.info('Starting extract_user')
    logger.info(f'auth_token:, {auth_token}')
    if auth_token is None:
        return None, None
    logger.info(auth_token)
    url = idm_url.generate_endpoint_url(f'/user?access_token={auth_token}')
    headers = {'Content-Type': 'application/json'}
    logger.info(url)
    logger.info(headers)
    response = requests.get(url, headers=headers).json()
    
    logger.info(response)
    username = response['username'].replace("@", ".")
    email = response['email']   
    logger.info(f'{username} and email {email}')
    logger.info('Finished extract_user')
    return username, email

def authenticate(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        logger.info(f"authenticate request: {request}")
        logger.info(f"authenticate, request.args: {request.args}")
        # logger.info(f"authenticate, username: {extract_user(request.args.get('auth_token'))}")
        auth_token = request.args.get('auth_token')
        logger.info(f"authenticate, auth_token: {auth_token}")
        username, _ = extract_user(auth_token)
        logger.info(f"authenticate, username: {username}")
        g.username = username
        
        if username is None:
            return jsonify({"error": "Unauthorized"}), 401  # return a 401 Unauthorized response
        
        return f(*args, **kwargs)
    
    return decorated_function

@app.route('/accept_terms', methods=['POST'])
def accept_terms():
    data = request.json
    username = data['username']
    # Update Redis to mark the user has accepted the terms
    redis_terms_client.set(username, 'true')
    logger.info(f"Accepting terms for: {username}")
    add_transaction(username, "TERMS", datetime.now().strftime('%Y-%m-%d %H:%M'), "ACCEPTED")
    return jsonify({'message': 'Terms accepted successfully'}), 200

def add_transaction(username, module, timestamp, message):
    logger.info("Starting add_transaction")
    logger.info(f"add_transaction, username:{username}, module:{module}, timestamp:{timestamp}, message:{message}")
    username = username.split('@')[0]
    transaction = f"{module} | {timestamp} | {message}"
    logger.info(f"User: {username} ADD Transaction: \t {transaction}")
    redis_historical_client.lpush(username, transaction)

@app.route("/transactions", methods=['GET'])
@authenticate
def get_transactions(start=0, end=-1):
    logger.info("Starting get_transactions")
    logger.info(f"User {g.username} GETting Transactions")
    transactions = redis_historical_client.lrange(g.username, start, end)
    logger.info(f"User {g.username} GET Transactions: \t {transactions}")
    return jsonify(transactions), 200

def obtain_analytic_toolbox_jwt(email, auth_token):
    payload = json.dumps({
        "emailId": email
    })
    headers = {
        'provider': 'BD4NRG-IDM',
        'Content-Type': 'application/json',
        'Authorization': f'Bearer {auth_token}'
    }
    logger.info("Obtaining jwt token")
    logger.info(f"Auth token: {auth_token}")
    logger.info(f"Payload: {payload}")

    response = requests.request("POST", at_url.generate_endpoint_url("/api/oauth/login"), headers=headers, data=payload)
    logger.info(f"Response: \n {response.text}")
    if response.status_code == 200:
        response_json = response.json()
        return response_json
    else:
        logger.error(f"Failed to obtain JWT token: {response.status_code} {response.text}")
        return None  # Or handle this case differently, e.g., raise an exception

def authenticate_analytic_toolbox(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        auth_token = request.args.get('auth_token')
        username, email = extract_user(auth_token)
        g.username = username
        g.email = email
        
        if username is None:
            return jsonify({"error": "Unauthorized"}), 401  # return a 401 Unauthorized response
        jwt = obtain_analytic_toolbox_jwt(email, auth_token)
        if jwt is not None:
            g.jwt = jwt['jwtToken']
            logger.info(f"jwt token: {jwt['jwtToken']}")
            return f(*args, **kwargs)
        else:
            return jsonify({"error": "Failed to obtain JWT token"}), 500  # Or some other error handling
    
    return decorated_function

def initialize_minio_client():
    try:
        minio_client = Minio(
                'minio:9000',
                access_key=minio_root_user,
                secret_key=minio_root_password,
                secure=False
        )
        logger.info("Minio client in url " + 'minio:9000' + " initialized with access_key: " + minio_root_user + " and secret_key: " + minio_root_password)
        return minio_client
    except:
        logger.error("Failed to initialize minio client in url " + 'minio:9000' + " with access_key: " + minio_root_user + " and secret_key: " + minio_root_password)
        return None

#Analytic Toolbox
def onboard_model(model_name, docker_file_url, protobuf_filename, protobuf_object, jwt_token):
    logger.info("Starting onboard_model")
    payload = {}
    # Create a BytesIO object from the protobuf_object's data
    protobuf_file = BytesIO(protobuf_object.data)
    files = [
        ('protobuf', (protobuf_filename, protobuf_file, 'application/octet-stream'))
    ]
    headers = {
        'modelName': model_name ,
        'dockerFileUrl': docker_file_url,
        'isCreateMicroservice': 'false',
        'Authorization': f'Bearer {jwt_token}'
    }

    response = requests.request(
        "POST", 
        at_url.generate_endpoint_url("/onboarding-app/v2/advancedModel"), 
        headers=headers, 
        data=payload, 
        files=files
    )
    logger.info("Onboarding model")
    logger.info(response.text)
    return response.text, response.status_code, response.headers.items()

@app.route("/analytics/onboard", methods=['POST'])
@authenticate_analytic_toolbox
def analytics_onboard():
    logger.info("Starting analytics_onboard")
    jwt_token = g.jwt
    data = request.get_json()

    protobuf_file = data.get('proto')
    asset = data.get('asset')
    model_name = data.get('modelName')


    minio_client = initialize_minio_client()
    if minio_client is None:
        return 'error: failed to initialize minio client with access_key: ' + minio_root_user + ' and secret_key: ' + minio_root_password, 500
    if minio_client.bucket_exists('protobuf'):
        try:
            logger.info("Trying to reach bucket name: " + 'protobuf' + " for resource: " + protobuf_file)
            proto_object = minio_client.get_object('protobuf', protobuf_file)
        except error.S3Error as err:    
            logger.error("S3Error when retrieving object")
            return 'error: S3Error when retrieving object', 500
        except:
            logger.error("Failed to retrieve object from minio")
            return 'error: failed to retrieve object from minio', 500
    else:
        return 'error: bucket does not exist', 500

    response_content, status_code, headers = onboard_model(model_name, asset, protobuf_file, proto_object, jwt_token)
    # Create a Flask response object
    flask_response = Response(response_content, status=status_code)
    add_transaction(g.username, "Analytic Toolbox", datetime.now().strftime('%Y-%m-%d %H:%M'), f"ONBOARDED {model_name}")
    # Copy headers from the onboard_model response to the Flask response object
    for header, value in headers:
        flask_response.headers[header] = value
    return flask_response

# DataAccess Service
@app.route("/das/data/official/list", methods=['GET'])
@authenticate
def das_data_get_official_list():
    logger.info("Starting das_data_get_official_list")
    bucket_name = 'official'
    minio_client = initialize_minio_client()
    if minio_client is None:
        return 'error: failed to initialize minio client with access_key: ' + minio_root_user + ' and secret_key: ' + minio_root_password, 500
    
    if minio_client.bucket_exists(bucket_name):
        try:
            logger.info("Trying to reach bucket name: " + bucket_name)
            response = minio_client.list_objects(bucket_name, recursive=True)

            datasets = []

            for obj in response:
                logger.info(obj.object_name)
                # info = minio_client.stat_object(bucket_name, obj.object_name).metadata
                # logger.info(f"Object read: {info}")
                # data = {
                #     "filename": obj.object_name,
                #     "upload_date": info['Date']
                # }
                datasets.append(obj.object_name)

            return jsonify(datasets), 200
        except:
            return 'error: failed to retrieve object from minio', 500
    else:
        logger.info("Bucket does not exist, creating it")
        try:
            minio_client.make_bucket(bucket_name)
            logger.info("Finished das_data_get_official_list")
        except error.ResponseError as err:
            logger.error("ResponseError when creating bucket")
            return 'error: ResponseError when creating bucket', 500
        return 'The bucket {bucket_name} was created', 201

@app.route("/das/data/official/download", methods=['POST'])
@authenticate
def das_data_get_official():
    logger.info("Starting das_data_get_official")
    bucket_name = 'official'
    data = request.get_json()
    object_name = data.get('object_name') 
    minio_client = initialize_minio_client()
    if minio_client is None:
        return 'error: failed to initialize minio client with access_key: ' + minio_root_user + ' and secret_key: ' + minio_root_password, 500
    if minio_client.bucket_exists(bucket_name):
        try:
            logger.info(f"Trying to reach bucket name: {bucket_name} for resource: {object_name}")
            minio_response = minio_client.get_object(bucket_name, object_name)

            add_transaction(g.username, "DAAS OFFICIAL", datetime.now().strftime('%Y-%m-%d %H:%M'), f"DOWNLOADED {object_name}")
                        
            def generate():
                for chunk in minio_response.stream():
                    yield chunk

            return Response(
                stream_with_context(generate()),
                200,
                {
                    'Content-Type': 'application/octet-stream',
                    'Content-Disposition': f'attachment;filename={object_name}'
                }
            )

        except error.S3Error as err:    
            logger.error("S3Error when retrieving object")
            logger.error(f"Exception: {str(e.message)}")  # Log the exception itself, to get more detailed info
            return 'error: S3Error when retrieving object', 500
        except Exception as e:
            logger.error("Failed to retrieve object from minio")
            logger.error(f"Exception: {str(e.message)}")  # Log the exception itself, to get more detailed info
            return 'error: failed to retrieve object from minio', 500
    logger.info("Finished das_data_get_official")
    return f'The object {object_name} or the bucket {bucket_name} does not exist', 500

@app.route("/das/data/user/list", methods=['GET'])
@authenticate
def das_data_get_user_list():   
    logger.info("Starting das_data_get_user_list")
    bucket_name = g.username
    minio_client = initialize_minio_client()
    if minio_client is None:
        return 'error: failed to initialize minio client with access_key: ' + minio_root_user + ' and secret_key: ' + minio_root_password, 500
    
    if minio_client.bucket_exists(bucket_name):
        try:
            logger.info("Trying to reach bucket name: " + bucket_name)
            # response = minio_client.list_objects(bucket_name, prefix="data/", recursive=True) 
            response = minio_client.list_objects(bucket_name, recursive=True)

            datasets = []

            for obj in response:
                logger.info(obj.object_name)
                # info = minio_client.stat_object(bucket_name, obj.object_name).metadata
                # logger.info(f"Object read: {info}")
                # data = {
                #     "filename": obj.object_name,
                #     "upload_date": info['Date']
                # }
                datasets.append(obj.object_name)
            logger.info(f"Finished das_data_get_user_list, jsonify(datasets):{jsonify(datasets)}")
            return jsonify(datasets), 200
        except:
            return 'error: failed to retrieve object from minio', 500
    else:
        logger.info("Bucket does not exist, creating it")
        try:
            minio_client.make_bucket(bucket_name)
        except error.ResponseError as err:
            logger.error(f"ResponseError when creating bucket: {str(e.message)}")  # Log the exception itself, to get more detailed info
            return 'error: ResponseError when creating bucket', 500
        return 'The bucket {bucket_name} was created', 201

@app.route("/das/data/user/download", methods=['POST'])
@authenticate
def das_data_get_user():
    logger.info("Starting das_data_get_user")
    bucket_name = g.username
    data = request.get_json()
    object_name = data.get('object_name')
    minio_client = initialize_minio_client()
    if minio_client is None:
        return 'error: failed to initialize minio client with access_key: ' + minio_root_user + ' and secret_key: ' + minio_root_password, 500
    
    if minio_client.bucket_exists(bucket_name):
        try:
            logger.info(f"Trying to reach bucket name: {bucket_name} for resource: {object_name}")
            minio_response = minio_client.get_object(bucket_name, object_name)
            
            def generate():
                for chunk in minio_response.stream():
                    yield chunk

            add_transaction(g.username, "DAAS", datetime.now().strftime('%Y-%m-%d %H:%M'), f"DOWNLOADED {object_name}")
            return Response(
                stream_with_context(generate()),
                200,
                {
                    'Content-Type': 'application/octet-stream',
                    'Content-Disposition': f'attachment;filename={object_name}'
                }
            )
        except error.S3Error as err:    
            logger.error("S3Error when retrieving object")
            return 'error: S3Error when retrieving object', 500
        except:
            logger.error("Failed to retrieve object from minio")
            return 'error: failed to retrieve object from minio', 500
    logger.info("Finished das_data_get_user")
    return f'The object {object_name} or the bucket {bucket_name} does not exist', 500

@app.route('/das/data/user', methods=['POST'])
@authenticate
def das_data_post():
    logger.info("Starting das_data_post")

    bucket_name = g.username

    logger.info(f"bucket_name: {bucket_name}")

    file = request.files.get('file')
    logger.info("Received file to upload to minio \t filename: " + file.filename + "\t content_type: " + file.content_type)
    
    minio_client = initialize_minio_client()
    if minio_client is None:
        return 'error: failed to initialize minio client with access_key: ' + minio_root_user + ' and secret_key: ' + minio_root_password, 500

    # checking if the bucket exists
    logger.info("Before checking if bucket " + bucket_name + "exists")
    logger.info(minio_client)

    if not minio_client.bucket_exists(bucket_name):
        logger.info("Bucket does not exist, creating it")
        try:
            minio_client.make_bucket(bucket_name)
        except error.ResponseError as err:
            logger.error("ResponseError when creating bucket")
            return 'error: ResponseError when creating bucket', 500

    logger.info("Before uploading file to minio")
    try:
        logger.info("Uploading file to minio")
        metadata = {
            'provider': g.username,
            'filename': file.filename,
            'uploaddate': datetime.utcnow(),
            'datatype': 'data'
        }
        logger.info(metadata)
        add_transaction(g.username, "DAAS", datetime.now().strftime('%Y-%m-%d %H:%M'), f"UPLOADED {file.filename}")
        minio_client.put_object(bucket_name, file.filename, file, length=-1, content_type=file.content_type, part_size=10*1024*1024, metadata=metadata)
        logger.info("Finished das_data_post")
        return 'File uploaded successfully' , 200
    except error.ResponseError as err:
        logger.error(f"ResponseError when pushing object: {str(e.message)}")  # Log the exception itself, to get more detailed info
        return 'error: ResponseError when pushing object', 500


@app.route('/das/data/user', methods=['DEL'])
@authenticate
def das_data_del():
    logger.info("Starting das_data_del")

    bucket_name = g.username
    file_name = request.args.get('filename')

    logger.info(f"bucket_name: {bucket_name}")
    logger.info("Received file to delete from minio \t filename: " + file_name.filename + "\t content_type: " + file_name.content_type)
    
    minio_client = initialize_minio_client()
    if minio_client is None:
        return 'error: failed to initialize minio client with access_key: ' + minio_root_user + ' and secret_key: ' + minio_root_password, 500

    logger.info("Before deleting file from minio")
    try:
        minio_client.remove_object(bucket_name, file_name)
        add_transaction(g.username, "DAAS", datetime.now().strftime('%Y-%m-%d %H:%M'), f"DELETED {file_name}")
        logger.info("Finished das_data_del")
        return 'File deleted successfully' , 200
    except error.ResponseError as err:
        logger.error(f"ResponseError das_data_del: {str(e.message)}")  # Log the exception itself, to get more detailed info
        return 'error: ResponseError when deleting object', 500


# AnomalyDetection Service -------------------------------------------------------------------------------------------
@app.route("/bd4nrg/ads/version", methods=['GET'])
def ads_version():
    try:
        response = requests.get(ads_url.generate_endpoint_url("/v1/ads/anomaly"))
        logger.info("Requested version to ADS module with response " + response.text)
        return response.text, response.status_code
    except:
        logger.error(f"Failed to retrieve version from the ADS module: {str(e.message)}")  # Log the exception itself, to get more detailed info
        return 'error: failed to retrieve version from the ADS module', 500

@app.route("/bd4nrg/ads/detect", methods=['POST'])
@authenticate
def ads_detect():
    logger.info("Starting ads_detect")
    username = g.username
    logger.info(f"username: {username}")
    minio_client = None
    try:
        logger.info("Initialized ads process")
        json_data = request.get_json()
        logger.info("json_data: " + json.dumps(json_data))
        fileName = json_data['fileName'] 
        logger.info("fileName: " + fileName)

        minio_client = initialize_minio_client()
        if minio_client is None:
            return 'error: failed to initialize minio client with access_key: ' + minio_root_user + ' and secret_key: ' + minio_root_password, 500

        #Obtain data and model from Minio
        if minio_client.bucket_exists(username):
            logger.info("Trying to reach bucket name: " + username + " for resource: " + fileName)
            data_response = minio_client.get_object(username, fileName) 

        logger.info("Downloaded object")
        temp_file_name = "/tmp/" + fileName
        with open(temp_file_name, 'wb') as file_data:
            for d in data_response.stream(32*1024):
                file_data.write(d)
        logger.info("Temp file written to disk")
        url = ads_url.generate_endpoint_url("/v1/source/local/" + fileName)
        files = {'file': (fileName, open(temp_file_name, 'rb'))}

        response = requests.put(url, files=files)
        logger.info("response.text (requests.put):" + response.text)
                    
        # Close and delete the temporary file
        os.remove(temp_file_name)
        logger.info("Deleted temp file")

        # Handle the response from the second server
        if response.status_code == 201:
            logger.info("File uploaded successfully to ads")
        else:
            logger.error("Error uploading file to ads: " + response.text)

        # Get the current configuration
        config_url = ads_url.generate_endpoint_url("/v1/ads/config")
        config_response = requests.get(config_url)
        if config_response.status_code != 200:
            logger.error("Failed to get the current configuration")
            return 'error: failed to retrieve configuration', 500

        current_config = config_response.json()
        logger.info("Config: \n" + str(current_config))
        # logger.info("JSON_DATA:" + json.dumps(json_data))
        logger.info("CURRENT_CONFIG:" + json.dumps(current_config))
        logger.info("Assigning...")
        # Modify the source.local field
        current_config["source"]["local"] = fileName
        # Update the configuration using the PUT method
        put_response = requests.put(config_url, json=current_config)
        if put_response.status_code != 201:
            logger.error("Failed to update the configuration")
            return 'error: failed to update configuration', 500
        logger.info("response (requests.put) - 2:" + put_response.text)

        # Initialize workers
        workers_url = ads_url.generate_endpoint_url("/v1/ads/workers")
        workers_response = requests.post(workers_url)
        if workers_response.status_code not in (200, 201):
            logger.error("Failed to initialize workers")
            return 'error: failed to initialize workers', 500
        logger.info("workers_response.text:" + workers_response.text) 

        # Call the detect API
        detect_url = ads_url.generate_endpoint_url("/v1/ads/detect")
        detect_payload = {
            "loop": 0,
            "period": 10
        }
        # detect_payload = {
        #     "loop": False,
        #     "period": 10
        # }
        # detect_payload = {
        #     "loop": 'False',
        #     "period": 10
        # }

        response = requests.post(detect_url, json=detect_payload)
        json_response = response.json()
        logger.info("response.text (requests.post):" + response.text)

        job_id = json_response['job_id']
        add_transaction(g.username, "ADS", datetime.now().strftime('%Y-%m-%d %H:%M'), f"DETECTION with job id {job_id}")
        add_redis_job_ads(username, job_id)
        logger.info("Finished ads_detect")
        return response.text, response.status_code
    except error.S3Error as err:    
        logger.error("S3Error when retrieving object")
        return 'error: S3Error when retrieving object', 500
    except:
        logger.error(f"Error in ads_detect: {str(e.message)}")  # Log the exception itself, to get more detailed info
        return 'error: Error in ads_detect', 500

@app.route("/bd4nrg/ads/cycles", methods=['GET']) # It filters by user
@authenticate
def get_cycles_list():
    try:
        logger.info("Starting get_cycles_list")  
        # TODO {ads} return cycles in order to see downloadable results
        request_cycles_url = ads_url.generate_endpoint_url("/v1/source/local/cycles") 
        request_cycles_response = requests.get(request_cycles_url)
        logger.info(f"request_cycles_response {request_cycles_response}")
        if request_cycles_response.status_code != 200:
            logger.error("Failed to get the executed cycles")
            return 'error: failed to retrieve cycles', 500

        logger.info(f"get_cycles_list , text:{request_cycles_response.text}, status_code:{request_cycles_response.status_code}")

        get_redis_jobs_ads_by_username = get_redis_jobs_ads(g.username)
        logger.info(f"get_redis_jobs_ads by username: {get_redis_jobs_ads_by_username}")

        all_jobs = json.loads(request_cycles_response.text)    
        logger.info(f"all_jobs:{all_jobs}")

        result=[]
        for aux in get_redis_jobs_ads_by_username: 
            logger.info(f"aux:{aux}")  
            job_id_from_log = _extract_job_id_from_redis_jobs_ads_logs(aux)
            logger.info(f"job_id_from_log:{job_id_from_log}")  
            for aux2 in all_jobs['cycles']: 
                logger.info(f"aux2:{aux2}")
                if job_id_from_log in aux2:
                    result.append(aux2)
        
        logger.info(f"result:{result}") # job list
        data = {}
        data['cycles'] = result
        json_data = json.dumps(data)
        logger.info(f"json_data:{json_data}") # filtered job list

        logger.info(f"Finished get_cycles_list")
    except Exception as e:
        logger.error(f"Error get_cycles_list: {str(e.message)}")  # Log the exception itself, to get more detailed info
        logger.error(f"Exception: {str(e.message)}")  # Log the exception itself, to get more detailed info

    return json_data, request_cycles_response.status_code

@app.route("/bd4nrg/ads/cycle", methods=['GET'])
@authenticate
def get_cycle():
    try:
        logger.info("Starting get_cycle")
        logger.info("request.args.get('cycle'):" + request.args.get('cycle'))
        
        cycle = request.args.get('cycle')  
        if not cycle:
            return 'error: cycle parameter missing', 400

        logger.info("cycle:" + cycle)
        # GET /v1/source/local/<job_id>/latest
        job_id = _extract_job_id_from_cycle(cycle)
        logger.info(f"get_cycle job_id extracted:{job_id}")
        request_job_url = ads_url.generate_endpoint_url(f"/v1/source/local/cycles/{job_id}/latest")
        request_job_response = requests.get(request_job_url)
        logger.info("request_job_response.status_code:" + str(request_job_response.status_code))
        if request_job_response.status_code != 200:
            logger.error("Failed to get the cycle")
            return 'error: failed to retrieve cycle', 500
        logger.info("request_cycle_response:" + request_job_response.text)
        logger.info("Finished get_cycle")
    except Exception as e:
        logger.error("Error get_cycle")
        logger.error(f"Exception: {str(e.message)}")  # Log the exception itself, to get more detailed info
    return request_job_response.text, request_job_response.status_code



# ExplainableAI Service -------------------------------------------------------------------------------------------

#FIXME: NOT USED
@app.route("/bd4nrg/eais/jobs", methods=['GET']) # It filters by user
def get_eais_jobs():
    logger.info("Starting get_eais_jobs")
    username = g.username

    get_redis_jobs_eais_by_username = get_redis_jobs_eais(g.username)
    logger.info(f"get_redis_jobs_eais_by_username: {get_redis_jobs_eais_by_username}")

    url_string = "/v1/shap/jobs"
    logger.info(f"url_string:{url_string}")
    response_jobs_list = requests.get(eais_url.generate_endpoint_url(url_string))
    # {
    #     "failed": [],
    #     "finished": [
    #         "c10d088f-a75a-4af9-a75b-1bc71e1e8931",
    #         "82ba416c-f8e5-48dd-8a98-30488a2a471c"
    #     ],
    #     "queued": [],
    #     "started": []
    # }
    
    result = []
    for aux in get_redis_jobs_eais_by_username: 
        logger.info(f"aux:{aux}")  
        job_id_from_log = _extract_job_id_from_redis_jobs_eais_logs(aux)
        logger.info(f"job_id_from_log:{job_id_from_log}")  
        for aux2 in response_jobs_list['finished']: 
            logger.info(f"aux2-finished:{aux2}")
            if job_id_from_log in aux2:
                result.append(aux2)
        for aux2 in response_jobs_list['queued']: 
            logger.info(f"aux2-queued:{aux2}")
            if job_id_from_log in aux2:
                result.append(aux2)
        for aux2 in response_jobs_list['started']:
            logger.info(f"aux2-started:{aux2}")
            if job_id_from_log in aux2:
                result.append(aux2)

    logger.info(f"result:{result}") # job list
    data = {}
    data['jobs'] = result
    json_data = json.dumps(data)
    logger.info(f"json_data:{json_data}") # filtered job list
    logger.info("Finished get_eais_jobs")
    return json_data, 200


# @app.route("/eais/data", methods=['GET'])
# def eais_data_get():
#     logger.info("Starting eais_data_get")
#     try:
#         response = requests.get(eais_url.generate_endpoint_url("/v1/data"))
#         logger.info("Requested data to EAIS module with response " + response.text)
#         logger.info("Finished eais_data_get")
#         return response.text, response.status_code
#     except:
#         logger.error("Failed to retrieve data from the EAIS module")
#         return 'error: failed to retrieve data from the EAIS module', 500

# @app.route("/eais/data", methods=['PUT'])
# def eais_data_put():
#     logger.info("Starting eais_data_put")
#     if 'file' not in request.files or request.files['file'] is None or request.files['file'].filename == "" or request.files['file'].content_type == "":
#         return 'error: Invalid file provided', 400

#     file = request.files['file']
#     files = [("file", (file.filename, file, file.content_type))]
#     try:
#         response = requests.put(eais_url.generate_endpoint_url("/v1/data"), files=files)
#         logger.info("File sent to eais data module \t filename: " + file.filename + "\t content_type: " + file.content_type)
#         logger.info("Finished eais_data_put")
#         return response.text, response.status_code
#     except:
#         logger.error("Failed to send file to eais data module \t filename: " + file.filename + "\t content_type: " + file.content_type)
#         return 'error: failed to send file to eais data module', 500


@app.route("/bd4nrg/eais/generate", methods=['GET'])
@authenticate
def get_generate_plot():
    try:
        logger.info("Starting get_generate_plot")
        username = g.username
        job_id = request.args.get('job_id')  
        plot_type = request.args.get('plot_type') 
        filename = request.args.get('filename')
        logger.info("job_id:" + job_id)
        logger.info("plot_type:" + plot_type)
        logger.info("fileName:" + filename)

        url_string = "/v1/shap/jobs/" + job_id
        logger.info(f"url_string:{url_string}")
        response = requests.get(eais_url.generate_endpoint_url(url_string))
        logger.info(f"response.text:{response.text}")
        logger.info(f"type(response):{type(response)}")
        response_text_json = json.loads(response.text)
        logger.info(f"type(response_json):{type(response_text_json)}")
        logger.info(f"response_text_json['finished']:{response_text_json['finished']}")

        # Start plot generation (when "finished": true) -------------------------
        tries = 0
        job_finished = False
        while (tries < 30) and (response_text_json['finished'] != True):
            response = requests.get(eais_url.generate_endpoint_url(url_string))
            logger.info(f"Esto es el response.text (bucle:{tries}):{response.text}")
            response_text_json = json.loads(response.text)
            if (response_text_json['finished'] == True):
                tries = 10 # salgo del bucle
                job_finished = True
            else:
                # Wait a few seconds before retrying and hope the problem goes away
                time.sleep(10) 
                tries += 1
            
        logger.info(f"job_finished:{job_finished}")
        if job_finished == True :
            eais_plot_generation_query = {
                "data": filename,  
                "id": job_id,
                "plot_type": {
                    "type": plot_type,
                    "ind": 0,
                    "inde": 20
                }
            }
            logger.info(f"Invoking a plot generation")
            response = requests.post(eais_url.generate_endpoint_url("/v1/shap/explainer/xgb.joblib/plot"), json=eais_plot_generation_query)
            logger.info(f"response:{response}")
            logger.info("Requested post generation plot to EAIS module with response " + response.text) 
    except:
        logger.error(f"Error in get_generate_plot: {str(e.message)}")  # Log the exception itself, to get more detailed info
        return 'error: get_generate_plot', 500

    logger.info("Finished get_generate_plot")
    return response.text, response.status_code  # returns 'GET result by job' OR 'Plot list of pdf files (force_plot)' 


@app.route("/bd4nrg/eais/explain", methods=['POST'])
@authenticate
def eais_explain():
    logger.info("Starting eais_explain")
    username = g.username
    minio_client = None
    try:
        logger.info("Initialized explain process")
        json_data = request.get_json()
        logger.info("Esto es el json_data:" + json.dumps(json_data))
        filename = json_data['filename']
        model = json_data['model']
        explainer = json_data['explainer']


        minio_client = initialize_minio_client()
        if minio_client is None:
            return 'error: failed to initialize minio client with access_key: ' + minio_root_user + ' and secret_key: ' + minio_root_password, 500

        #Obtain data and model from Minio
        if minio_client.bucket_exists(username):
            logger.info("Trying to reach bucket name: " + username + " for resource: " + filename)
            data_response = minio_client.get_object(username, filename)
            logger.info("Trying to reach bucket name: " + username + " for resource: " + model)
        logger.info("Correctly retrieved data and model from Minio")
        # Create a binary stream object from the downloaded file data
        file_data = BytesIO(data_response.data)
        # model_data = BytesIO(model_response.data)
        logger.info("Correctly created binary stream objects from the downloaded file data")
        # Use the binary stream object as the `file` parameter in the tuple
        files = [("file", (filename, file_data, 'text/csv'))]
        # model = [("file", (model, model_data, 'unknown'))]
        logger.info("Created object from steam data: " + str(files))
        # logger.info("Created object from steam model: " + str(model))
        response = requests.put(eais_url.generate_endpoint_url("/v1/data"), files=files)
                # {
                #     "message": "data upload successfull"
                # }
        logger.info("Requested put data to EAIS module with response " + str(response.text))
        # Start workers ---------------------------
        response = requests.post(eais_url.generate_endpoint_url("/v1/shap/workers"))
        logger.info("Requested workers to EAIS module in order to start a job with response " + response.text +  " with status code: " 
                    +  str(response.status_code))            
        
        # Start explainer -------------------------
        eais__body_query = {
            'data': filename, 
            'explainer': explainer
        }
        url_string = eais_url.generate_endpoint_url("/v1/shap/explainer/" + model) # /v1/shap/explainer/xgb.joblib
        logger.info(f"url_string:{url_string}")
        logger.info(f"eais__body_query:{eais__body_query}")
        response = requests.post(url_string, json=eais__body_query)
        logger.info("Requested post jobs (request explainer) to EAIS module with response " + response.text)
        json_response = response.json()
        job_id = json_response["job_id"]
        logger.info(f"json_id:{job_id}")

        add_transaction(g.username, "EAIS", datetime.now().strftime('%Y-%m-%d %H:%M'), f"EXPLAIN with job id {job_id}")
        add_redis_job_eais(username, job_id)
        logger.info("Finished eais_explain")
        return response.text, response.status_code
    except error.S3Error as err:    
        logger.error(f"S3Error when retrieving object: {str(e.message)}")  # Log the exception itself, to get more detailed info
        return 'error: S3Error when retrieving object', 500
    except:
        logger.error(f"Error in eais_explain: {str(e.message)}")  # Log the exception itself, to get more detailed info
        return 'error: Error in eais_explain', 500



@app.route("/bd4nrg/eais/plots", methods=['GET'])
@authenticate
def get_plots_list():
    logger.info("Starting get_plots_list")
    username = g.username
    url_string = "/v1/shap/explainer/xgb.joblib/plot"
    logger.info(f"url_string:{url_string}")
    response = requests.get(eais_url.generate_endpoint_url(url_string))
    logger.info("Finished get_plots_list")
    return response


@app.route("/bd4nrg/eais/plot", methods=['GET'])
@authenticate
def get_eais_plot():
    try:
        logger.info("Starting get_eais_plot")
        # username = g.username
        file_name = request.args.get('file_name')  
        # file_name = 'force_plot_xgb.joblib_b25d33a9-dc77-4f96-98bf-d19a8e24908e_2.pdf' # for testing purposes
        logger.info("file_name:" + file_name)

        url_string = "/v1/shap/explainer/xgb.joblib/plot/" + file_name
        logger.info(f"url_string:{url_string}")
        response = requests.get(eais_url.generate_endpoint_url(url_string), stream=True)
        def generate():
            for chunk in response.iter_content(chunk_size=128):
                yield chunk

        logger.info("Finished get_eais_plot")
        return Response(stream_with_context(generate()), 200,
                {
                    'Content-Type': 'application/octet-stream',
                    'Content-Disposition': f'attachment;filename={file_name}'
                }
        )
    except:
        logger.error("Error in get_eais_plot")
        return 'error: Error in get_eais_plot', 500


@app.route("/eais/models", methods=['GET'])
@authenticate
def eais_get_models():
    try:
        logger.info("Starting eais_get_models")
        
        url_string = eais_url.generate_endpoint_url("/v1/models")
        logger.info(f"url_string:{url_string}")
        response = requests.get(url_string)

        logger.info("response.text:" + response.text)
        logger.info("Finished eais_get_models")
    except Exception as e:
        logger.error("Error eais_get_models")
        logger.error(f"Exception: {str(e.message)}")  # Log the exception itself, to get more detailed info
        return 'Error eais_get_models', 500
    return response.text, response.status_code


# @app.route("/eais/model", methods=['PUT'])
# def eais_model_put():
#     try:
#         logger.info("Starting eais_model_put")
#         if 'file' not in request.files or request.files['file'] is None or request.files['file'].filename == "" or request.files['file'].content_type == "":
#             return 'error: Invalid file provided', 400

#         file = request.files['file']
#         files = [("file", (file.filename, file, file.content_type))]
        
#         response = requests.put(eais_url.generate_endpoint_url("/v1/models"), files=files)
#         logger.info("File sent to eais model module \t filename: " + file.filename + "\t content_type: " + file.content_type)
#         logger.info("Finished eais_model_put")
#     except Exception as e:
#         logger.error("Error eais_model_put: " + e)
#         return 'Error eais_model_put', 500
#     return response.text, response.status_code



# Augmentation Service -------------------------------------------------------------------------------------------
@app.route("/bd4nrg/augs/augment", methods=['POST'])
@authenticate
def augs_augment():
    logger.info("Starting augs_augment")
    username = g.username
    logger.info(f"augs_augment username:{username}")
    try:
        logger.info("Initialized augmentation process")
        json_data = request.get_json() 
        logger.info("json.dumps(json_data):" + json.dumps(json_data))
        common_fileName = json_data['fileName'] 
        wildcard_name = common_fileName + "_*.*"

        # {augs} GET /v1/source/remote/config
        response = requests.get(augs_url.generate_endpoint_url("/v1/source/remote/config"))
        config_data = response.json()
        logger.info(config_data)

        # Modify the config values
        config_data['minio']['access_key'] = minio_root_user
        config_data['minio']['secret_key'] = minio_root_password
        config_data['minio']['endpoint'] = "minio:9000"
        config_data['minio']['secure'] = 0
        logger.info(f"config_data:{config_data}")

        # {augs} PUT /v1/source/remote/config
        response = requests.put(augs_url.generate_endpoint_url("/v1/source/remote/config"), json=config_data['minio'])
        logger.info(f"response-1:{response}")

        response = requests.get(augs_url.generate_endpoint_url("/v1/source/remote/config"))
        config_data = response.json()
        logger.info(config_data)
        logger.info(f"config_data:{config_data}")
        
        # {augs} GET /v1/augment/sequencer
        response = requests.get(augs_url.generate_endpoint_url("/v1/augment/sequencer"))
        sequencer_data = response.json()
        logger.info("json.dumps(sequencer_data): " + json.dumps(sequencer_data))

        sequencer_data['file_handling']['wild_card'] = wildcard_name 

        # Modify bucket_in and bucket_out values
        sequencer_data['source']['bucket_in'] = username
        sequencer_data['source']['bucket_out'] = username

        # {augs} PUT /v1/augment/sequencer
        logger.info(f"sequencer_data:{sequencer_data}")
        response = requests.put(augs_url.generate_endpoint_url("/v1/augment/sequencer"), json=sequencer_data)
        # logger.info(f"response augmenter/sequencer:{response.text}")
        logger.info(f"response augmenter/sequencer:{response}")

        # {augs} POST /v1/sequencer/worker
        response = requests.post(augs_url.generate_endpoint_url("/v1/sequencer/workers"))
        logger.info(f"response sequencer/workers:{response}")

        # {augs} POST /v1/sequencer
        logger.info(f"CALLING SEQUENCER - 1")
        response = requests.post(augs_url.generate_endpoint_url("/v1/sequencer"))
        logger.info(f"CALLING SEQUENCER - 2")
        logger.info(f"response SEQUENCER:{response}")
        logger.info(f"response SEQUENCER:{response.text}")
        json_response = response.json()
        logger.info(f"CALLING SEQUENCER - 3")
        job_id = json_response["job_id"]
        logger.info(f"job_id:{job_id}")
        logger.info(f"CALLING SEQUENCER - 4")
        add_transaction(g.username, "AUGS", datetime.now().strftime('%Y-%m-%d %H:%M'), f"AUGMENTATION with job id {job_id}")
        add_redis_job_augs(username, job_id)
        logger.info("Finished augs_augment")
        return response.text, 200
    except error.S3Error as err:
        logger.error("S3Error when retrieving object")
        return 'error: S3Error when retrieving object', 500
    except Exception as e:
        logger.error(f"Error in augs_augment")
        logger.error(f"Exception: {str(e)}")  # Log the exception itself, to get more detailed info
        return f'error: error in augs_augment: {e}', 500

# Status
@app.route('/status')
def home():
    return 'Application is live.'

#------------------------------- BLOCKCHAIN methods  -------------------------------

# @app.route("/bd4nrg/bc/receipts/last", methods=['GET']) 
# @authenticate
# def get_bc_last_receipts():
#     logger.info("Starting get_tp_last_receipts")  

#     response = requests.get(tp_url.generate_endpoint_url("/get-last-receipts"))

#     logger.info(f"Finished get_tp_last_receipts")
#     return response


def _retrieve_blockchain_keys_from_user(user: str):
    blockchain_users = {
        "arturo.medela" : {
            "public" : "0x037bB274D93f4Db069AC961d653a807E4E7c5A48",
            "private" : "dbbf87459bea5ca7309d9eb990f05922ab8579f4ad42364ffc229000db2ed818"
        },
        "borja.ruiz" : {
            "public" : "0xd16E92811948E986ee26c7759f7DA53BAa8DD980",
            "private" : "de4ed0ee59a7153b7c32e5d6e25ecbfd42e94f0130be604d231169d2e67cd7f5"
        },
        "lorcristof" : {
            "public" : "0xc47283Dc3A778D863ECc83fa30eC09C3BA339a93",
            "private" : "650ae2666d30ab625fd3236b9af986b2db8a2966955e16ef7c97dd80edec67a4"
        },
        "pedro.branco" : {
            "public" : "0x4510D109CA0B7fA54977E9aEBDa4Fad66AeB8802",
            "private" : "848ab379a594716f701331ba8e23ed989c6d95b81bb8b26f0db77ef73cb990f3"
        },
        "victor.serna" : {
            "public" : "0x82B738f0D434D6706c5C92AD60dfF4334f90Dc3c",
            "private" : "6d15bae4689666fa961f12f3d7eda18855192d52ef7b1ade75c0d2b7d47700aa"
        },
        "vkarakolis" : {
            "public" : "0xc8Dd7d6Fe4D7A32651222Ab529080228981848F4",
            "private" : "e84fd415f98de34c01343c27438ba80ce2e537ea359c686d8ffd0d6b0302da40"
        }
    }
    user_to_check = user
    public_key = None
    private_key = None
    for blockchain_user in blockchain_users:
        if user_to_check in blockchain_user:
            public_key = blockchain_users[blockchain_user]['public']
            private_key = blockchain_users[blockchain_user]['private']
    return public_key, private_key



@app.route("/xxx/yyy", methods=['GET'])
@authenticate
def account_initialization():
    try:
        logger.info("Starting account_initialization")

        # 1:0x037bB274D93f4Db069AC961d653a807E4E7c5A48 
        # 2:0xd16E92811948E986ee26c7759f7DA53BAa8DD980 
        # 3:0xc47283Dc3A778D863ECc83fa30eC09C3BA339a93 
        # 4:0x4510D109CA0B7fA54977E9aEBDa4Fad66AeB8802 
        # ...       
        counts = ['0x037bB274D93f4Db069AC961d653a807E4E7c5A48', '0xd16E92811948E986ee26c7759f7DA53BAa8DD980'
                  , '0xc47283Dc3A778D863ECc83fa30eC09C3BA339a93', '0x4510D109CA0B7fA54977E9aEBDa4Fad66AeB8802'
                  , '0x58aFcB861205ef496a90b9dFB9A30714f995967F', '0x82B738f0D434D6706c5C92AD60dfF4334f90Dc3c'
                  , '0xc8Dd7d6Fe4D7A32651222Ab529080228981848F4', '0xDE9Cc01662ea77c9b8635C94F1bCDE5B6D446153'
                  , '0x545DF5c81b4C7Dc1F75A4228B1E7A8d9068d6e1E']
        # amount = 100
        amount = request.args.get('amount')
        logger.info(f"amount:{amount}")

        for count in counts:
            bk_mint_body_query = {
                'to': count, 
                'amount': amount
            }
            url_string = tp_url.generate_endpoint_url("/mint")
            logger.info(f"url_string:{url_string}, body:{bk_mint_body_query}")

            response = requests.post(url_string, json=bk_mint_body_query)
            logger.info("response.text:" + response.text)

        logger.info("Finished account_initialization")
    except Exception as e:
        logger.error("Error account_initialization")
        logger.error(f"Exception: {str(e.message)}")  # Log the exception itself, to get more detailed info
        return 'Error account_initialization', 500
    return response.text, response.status_code


@app.route("/xxx/yyy/zzz", methods=['GET'])
@authenticate
def sells_initialization():
    try:
        logger.info("Starting sells_initialization")

        info = {
            "info": "sells_information",
            "sells": []
        }

        num_assets = request.args.get('num_assets')
        asset_price = request.args.get('asset_price')

        logger.info(f"num_assets:{num_assets}, asset_price:{asset_price}")

        num_assets_int = int(num_assets)
        # asset_price_int = int(asset_price)

        for asset in range(num_assets_int):
            asset_id = "asset_" + str(asset)
            bk_sell_body_query = {
                'id': asset_id, 
                'price': asset_price
            }
            url_string = tp_url.generate_endpoint_url("/sell")
            logger.info(f"url_string:{url_string}, body:{bk_sell_body_query}")

            response = requests.post(url_string, json=bk_sell_body_query)
            json_response = json.loads(response.text)
            logger.info(f"json.dumps(json_response):{json.dumps(json_response)}")
            signature = json_response['signature']
            hashedRefWithNonce = json_response['hashedRefWithNonce']
            receiptID = json_response['receiptID']
            price = json_response['price']

            result_aux_0 = {
                'signature': signature, 
                'hashedRefWithNonce': hashedRefWithNonce,
                'receiptID': receiptID,
                'price': price
            }
            result_aux_info = {
                'asset_id': asset_id, 
                'asset_sell_info': result_aux_0
            }

            info['sells'].append({
                "data": result_aux_info
            });

        result = json.dumps(info)
        logger.info(f"json_result:{result}")
        logger.info("Finished sells_initialization")
    except Exception as e:
        logger.error("Error sells_initialization")
        logger.error(f"Exception: {str(e.message)}")  # Log the exception itself, to get more detailed info
        return 'Error sells_initialization', 500
    
    return result, 200


@app.route("/bd4nrg/tp/mint", methods=['POST']) 
@authenticate
def post_bc_mint():
    try:
        logger.info("Starting post_tp_mint")  
        username = g.username

        json_data = request.json
        body_to = json_data['to']
        body_amount = json_data['amount']

        logger.info("json_data: " + json.dumps(json_data))
        
        bk_mint_body_query = {
            'to': body_to, 
            'amount': body_amount
        }
        url_string = tp_url.generate_endpoint_url("/mint")
        logger.info(f"url_string:{url_string}, body:{bk_mint_body_query}")

        response = requests.post(url_string, json=bk_mint_body_query)
        logger.info("response.text:" + response.text)
        add_transaction(username, "TP", datetime.now().strftime('%Y-%m-%d %H:%M'), f"MINT to:{body_to} amount:{body_amount}")
        logger.info(f"Finished post_tp_mint")
    except Exception as e:
        logger.error("Error post_tp_mint")
        logger.error(f"Exception: {str(e.message)}")  # Log the exception itself, to get more detailed info
        return 'Error post_tp_mint', 500
    return response.text, response.status_code


@app.route("/bd4nrg/tp/sell", methods=['POST']) 
@authenticate
def post_bc_sell():
    try:
        logger.info("Starting post_tp_sell")  
        username = g.username
        
        json_data = request.json
        asset_id = json_data['id']
        asset_price = json_data['price']

        logger.info("json_data: " + json.dumps(json_data))
        bk_sell_body_query = {
            'id': asset_id, 
            'price': asset_price
        }
        url_string = tp_url.generate_endpoint_url("/sell")
        logger.info(f"url_string:{url_string}, body:{bk_sell_body_query}")

        response = requests.post(url_string, json=bk_sell_body_query)
        json_response = json.loads(response.text)
        logger.info(f"json.dumps(json_response):{json.dumps(json_response)}")
        receiptID = json_response['receiptID']
        add_transaction(username, "TP", datetime.now().strftime('%Y-%m-%d %H:%M'), f"SELL asset id:{asset_id} asset price:{asset_price}, receipt id:{receiptID}")
        # add_transaction(username, "TP", datetime.now().strftime('%Y-%m-%d %H:%M'), f"SELL asset_id:{asset_id} asset_price:{asset_price}")
        logger.info(f"Finished post_tp_sell")
    except Exception as e:
        logger.error("Error post_tp_sell")
        logger.error(f"Exception: {str(e.message)}")  # Log the exception itself, to get more detailed info
        return 'Error post_tp_sell', 500
    return response.text, response.status_code


@app.route("/bd4nrg/tp/purchase", methods=['POST']) 
@authenticate
def post_bc_purchase():
    try:
        logger.info("Starting post_tp_purchase")  
        username = g.username

        # Harcoding buyer's public & private keys  
        public_key, private_key =_retrieve_blockchain_keys_from_user(username)
        logger.info(f"username:{username}, public_key:{public_key}, private_key:{private_key}")

        json_data = request.json
        signature = json_data['receipt']['signature']
        hashedRefWithNonce = json_data['receipt']['hashedRefWithNonce']
        receiptID = json_data['receipt']['receiptID']
        asset_name = json_data['receipt']['assetName']
        asset_id = json_data['receipt']['assetId']
        private_key_buyer = private_key
        public_key_buyer = public_key
        price = json_data['price']
        logger.info("json_data: " + json.dumps(json_data))
        
        bk_purchase_body_query_receipt = {
            'signature': signature, 
            'hashedRefWithNonce': hashedRefWithNonce,
            'receiptID': receiptID
        }
        bk_purchase_body_query = {
            'private_key_buyer': private_key_buyer, 
            'public_key_buyer': public_key_buyer,
            'receipt': bk_purchase_body_query_receipt,
            'price': price
        }

        url_string = tp_url.generate_endpoint_url("/purchase")
        logger.info(f"url_string:{url_string}, body:{bk_purchase_body_query}")

        response = requests.post(url_string, json=bk_purchase_body_query)

        logger.info("response.text:" + response.text)
        add_transaction(username, "TP", datetime.now()
                        .strftime('%Y-%m-%d %H:%M'), 
                        f"PURCHASE asset name:{asset_name}, receipt id:{receiptID}, public key buyer:{public_key_buyer}, price:{price}")
        logger.info(f"Finished post_tp_purchase")
    except Exception as e:
        logger.error("Error post_tp_purchase")
        logger.error(f"Exception: {str(e.message)}")  # Log the exception itself, to get more detailed info
        return 'Error post_tp_purchase', 500
    return response.text, response.status_code


@app.route("/bd4nrg/tp/purchase/verify", methods=['POST']) 
@authenticate
def post_bc_verify_purchase():
    try:
        logger.info("Starting post_tp_verify_purchase")  
        username = g.username
        
        json_data = request.json
        hashedRefWithNonce = json_data['hashedRefWithNonce']
        receiptID = json_data['receiptID']

        logger.info("json_data: " + json.dumps(json_data))
        
        # {"hashedRefWithNonce": "0x9f88fb6de499291d3f9113a7aaf2859ed576fee76282b020a6ef3bfb941d12ad", 
        #  "receiptID": "0x4a83a945b1997c7a8610c07dadfd15c53877c4ed45da7a359dcb418828309987"}
        bk_verify_purchase_body_query = {
            'hashedRefWithNonce': hashedRefWithNonce, 
            'receiptID': receiptID,

        }
        url_string = tp_url.generate_endpoint_url("/verify-purchase")
        logger.info(f"url_string:{url_string}, body:{bk_verify_purchase_body_query}")

        response = requests.post(url_string, json=bk_verify_purchase_body_query)

        logger.info("response.text:" + response.text)
        add_transaction(username, "TP", datetime.now().strftime('%Y-%m-%d %H:%M'), f"VERIFY PURCHASE receipt id:{receiptID}")
        logger.info(f"Finished post_tp_verify_purchase")
    except Exception as e:
        logger.error("Error post_tp_verify_purchase")
        logger.error(f"Exception: {str(e.message)}")  # Log the exception itself, to get more detailed info
        return 'Error post_tp_verify_purchase', 500
    return response.text, response.status_code


@app.route("/bd4nrg/tp/receipt", methods=['POST']) 
@authenticate
def post_bc_get_receipt():
    try:
        logger.info("Starting post_tp_get_receipt")  
        username = g.username
        
        json_data = request.json
        receiptID = json_data['receiptID']

        logger.info("json_data: " + json.dumps(json_data))

        # /get-receipt
        bk_get_receipt_body_query = {
            'receiptID': receiptID,

        }
        url_string = tp_url.generate_endpoint_url("/get-receipt")
        logger.info(f"url_string:{url_string}, body:{bk_get_receipt_body_query}")

        response = requests.post(url_string, json=bk_get_receipt_body_query)

        logger.info("response.text:" + response.text)
        add_transaction(username, "TP", datetime.now().strftime('%Y-%m-%d %H:%M'), f"RECEIPT receipt id:{receiptID}")
        logger.info(f"Finished post_tp_get_receipt")
    except Exception as e:
        logger.error("Error post_tp_get_receipt")
        logger.error(f"Exception: {str(e.message)}")  # Log the exception itself, to get more detailed info
        return 'Error post_tp_get_receipt', 500
    return response.text, response.status_code


@app.route("/bd4nrg/tp/balance", methods=['POST']) 
@authenticate
def post_bc_balance():
    try:
        logger.info("Starting post_tp_balance")  
        username = g.username

        json_data = request.json
        body_of = json_data['of']

        logger.info("json_data: " + json.dumps(json_data))
        
        bk_balance_body_query = {
            'of': body_of, 

        }
        url_string = tp_url.generate_endpoint_url("/balance")
        logger.info(f"url_string:{url_string}, body:{bk_balance_body_query}")

        response = requests.post(url_string, json=bk_balance_body_query)
        logger.info("response.text:" + response.text)
        add_transaction(username, "TP", datetime.now().strftime('%Y-%m-%d %H:%M'), f"BALANCE of:{body_of} result:{response.text}")
        logger.info(f"Finished post_tp_balance")
    except Exception as e:
        logger.error("Error post_tp_balance")
        logger.error(f"Exception: {str(e)}")  # Log the exception itself, to get more detailed info
        return 'Error post_tp_balance', 500
    return response.text, response.status_code


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5080)
