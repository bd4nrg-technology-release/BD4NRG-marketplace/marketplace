#!/bin/bash
export BD4NRG_NEXUS_SECRET=bd4nrg-nexus-credentials
export DIDI_NAMESPACE=bd4nrg
export PUBLIC_NODE_DOMAIN=marketplace.bd4nrg.eu

#export AGORA_NEXUS3_REGISTRY=$(cat dockerconfig.json)
DOCKER_USERNAME="borja.ruiz"
DOCKER_PASSWORD=""
DOCKER_REGISTRY="https://registry.atosresearch.eu:18474"
API_PORT=5080
API_CONTAINER_NAME='api'

export API_PORT=3000
export API_CONTAINER_NAME=api
export ADS_PORT=5005
export ADS_CONTAINER_NAME=ads
export EAIS_PORT=5006
export EAIS_CONTAINER_NAME=eais
export TP_PORT=5008
export TP_CONTAINER_NAME=tp_api
export MINIO_PORT=9000
export MINIO_HOST=minio
export MINIO_ROOT_USER=minio
export MINIO_ROOT_PASSWORD=minio123
export MINIO_DRUID_USER=druid
export MINIO_DRUID_PASSWORD=druid-htap-olap4-bd4nrg
export MINIO_AUTOML_USER=pycaret
export MINIO_AUTOML_PASSWORD=pycaret-automl4-bd4nrg
export MINIO_SERVER_HOST=minio

# Base64 encode the Docker configuration file
DOCKER_CONFIG_BASE64=$(echo "{\"auths\":{\"$DOCKER_REGISTRY\":{\"username\":\"$DOCKER_USERNAME\",\"password\":\"$DOCKER_PASSWORD\"}}}" | base64 -w 0)

# Store the base64-encoded string in the AGORA_NEXUS3_REGISTRY variable
export BD4NRG_NEXUS3_REGISTRY="$DOCKER_CONFIG_BASE64"