import pytest
import json
from server import app

import string
import random

def generate_random_string(length):
    characters = string.ascii_letters + string.digits
    random_string = ''.join(random.choice(characters) for _ in range(length))
    return random_string


@pytest.fixture
def client():
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client

def test_create_user(client):
    user = generate_random_string(10)
    response = client.post('/create_user', data=dict(username=user, password='newpassword', role='consumer'))
    assert response.status_code == 201
    assert json.loads(response.data)['message'] == 'User created successfully.'

def test_create_existing_user(client):
    user = generate_random_string(10)
    client.post('/create_user', data=dict(username=user, password='newpassword', role='consumer'))
    response = client.post('/create_user', data=dict(username=user, password='newpassword', role='consumer'))
    assert response.status_code == 409
    assert json.loads(response.data)['error'] == 'User already exists.'

# def test_create_user_invalid_role(client):
#     user = generate_random_string(10)
#     response = client.post('/create_user', data=dict(username=user, password='newpassword', role='invalid'))
#     assert response.status_code == 400
#     assert json.loads(response.data)['error'] == 'Invalid role. Must be "provider" or "consumer".'

# def test_remove_user(client):
#     username = generate_random_string(10)
#     # First, create a user to remove
#     client.post('/create_user', data=dict(username=username, password='password', role='consumer'))
#     # Test removing the user
#     response = client.delete(f'/api/admin/remove/{username}')
#     assert response.status_code == 200
#     assert json.loads(response.data)['message'] == 'User removed.'

def test_add_dataset(client):
    providerid = generate_random_string(10)
    response = client.post(f'/api/provider/{providerid}/add_dataset', data=dict(filename='dataset_file', bucket_name='bucket_name', dataset_type='dataset_type'))
    assert response.status_code == 200
    assert json.loads(response.data)['message'] == 'Dataset metadata added successfully.'

def test_add_service(client):
    providerid = generate_random_string(10)
    response = client.post(f'/api/provider/{providerid}/add_service', data=dict(filename='service_file', bucket_name='bucket_name', service_type='service_type'))
    assert response.status_code == 200
    assert json.loads(response.data)['message'] == 'Service metadata added successfully.'

def test_get_datasets(client):
    providerid = generate_random_string(10)
    client.post(f'/api/provider/{providerid}/add_dataset', data=dict(filename='dataset_file', bucket_name='bucket_name', dataset_type='dataset_type'))
    response = client.get(f'/api/provider/{providerid}/get_datasets')
    assert response.status_code == 200
    datasets = json.loads(response.data)
    assert isinstance(datasets, list)

def test_get_services(client):
    providerid = generate_random_string(10)
    client.post(f'/api/provider/{providerid}/add_service', data=dict(filename='service_file', bucket_name='bucket_name', service_type='service_type'))
    response = client.get(f'/api/provider/{providerid}/get_services')
    assert response.status_code == 200
    services = json.loads(response.data)
    assert isinstance(services, list)
