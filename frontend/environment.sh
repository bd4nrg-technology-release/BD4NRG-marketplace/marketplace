#!/bin/bash
export BD4NRG_NEXUS_SECRET=bd4nrg-nexus-credentials
export DIDI_NAMESPACE=bd4nrg
export PUBLIC_NODE_DOMAIN=marketplace.bd4nrg.eu

#export AGORA_NEXUS3_REGISTRY=$(cat dockerconfig.json)
DOCKER_USERNAME="borja.ruiz"
DOCKER_PASSWORD=""
DOCKER_REGISTRY="https://registry.atosresearch.eu:18474"

# Base64 encode the Docker configuration file
DOCKER_CONFIG_BASE64=$(echo "{\"auths\":{\"$DOCKER_REGISTRY\":{\"username\":\"$DOCKER_USERNAME\",\"password\":\"$DOCKER_PASSWORD\"}}}" | base64 -w 0)

# Store the base64-encoded string in the AGORA_NEXUS3_REGISTRY variable
export AGORA_NEXUS3_REGISTRY="$DOCKER_CONFIG_BASE64"