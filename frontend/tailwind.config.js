/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
  "./src/**/*.{js,ts,jsx,tsx}",
  "./app/**/*.js", 
  "./node_modules/flowbite-react/lib/**/*.js"
  ],
  theme: {
    extend: {
      colors: {
        projectBlack: {
          400: '#0F0F0F'
        },
        projectBlue: {
          50: '#0730AB',
          100: '#0838c7',
          200: '#0a45f5',
          300: '#1b53fa',
          400: '#2a5ffa',
          500: '#3769fa',
          600: '#4270f5',
          700: '#547ef7',
          800: '#648af5',
          900: '#82a1fa'
        },
        projectPink: {
          50: '#f74384',
          300: '#fa196a',
          400: '#DF0B57',
          500: '#c40a4d'
        }
      }
    },
  },
  darkMode: 'media',
  plugins: [
    require("flowbite/plugin")
  ],
}
