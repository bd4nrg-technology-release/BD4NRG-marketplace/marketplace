'use client'
import React, { useEffect, useState } from "react";
import './MyData.css';
import NotLoggedInPage from "../common/login/page.js";
import { Button, Modal, FileInput, Label } from 'flowbite-react';

function ModelSummary() {
  const [datasets, setDatasets] = useState([]);
  // const [modalIsOpen, setIsOpen] = useState(false);
  const [selectedFile, setSelectedFile] = useState(null);
  const [selectedDataset, setSelectedDataset] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [token, setToken] = useState('');

  const [openModal, setOpenModal] = useState(false);

  // There is no env, lets use this at least
  // HTTPS server
  const rootUrlServices = 'https://api.marketplace.bd4nrg.eu'
  // Local deploy
  // const rootUrlServices = 'http://localhost:5080'


  useEffect(() => {
    // sessionStorage will now be accessed only on the client-side
    if (typeof window !== 'undefined') {
      const storedToken = sessionStorage.getItem('access_token');
      if (storedToken) {
        setToken(storedToken);
      }
    }
  }, []);

  const handleFileChange = (event) => {
    setSelectedFile(event.target.files[0]);
  };

  const handleRowClick = (dataset) => {
    setSelectedDataset(dataset);
  };

  const handleSubmit = async () => {
    setIsLoading(true);
    const formData = new FormData();
    formData.append('file', selectedFile);

    const requestOptions = {
      method: 'POST',
      body: formData
    };

    const auth_token = sessionStorage.getItem('access_token');
    const response = await fetch(rootUrlServices + '/das/data/user?auth_token=' + auth_token, requestOptions);
    // No catch for errors... not good :/
    const result = await response.text();
    console.log(result)
    fetchData()
    setIsLoading(false)
    setOpenModal(false)
    // setIsOpen(false);
  };


  const handleDownload = async (dataset) => {

    const auth_token = sessionStorage.getItem('access_token');
    const request_url = new URL(rootUrlServices + '/das/data/user/download');
    request_url.search = new URLSearchParams({
      auth_token: auth_token,
    });

    const response = await fetch(request_url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        object_name: dataset,
      }),
    }
    );
    const blob = await response.blob();

    const url = window.URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', `${dataset}`);
    document.body.appendChild(link);
    link.click();
  };

  // const openModal = () => {
  //   setIsOpen(true);
  // };

  // const closeModal = () => {
  //   setIsOpen(false);
  // };

  const fetchData = async () => {
    const auth_token = sessionStorage.getItem('access_token');
    console.log(auth_token);
    const response = await fetch(rootUrlServices + '/das/data/user/list?auth_token=' + auth_token);
    const data = await response.json();
    const formattedData = data.map(filename => ({ filename }));
    console.log(formattedData)
    setDatasets(formattedData);
  }

  useEffect(() => {
    fetchData();
  }, []);

  if (!token) {
    return <NotLoggedInPage />;
  }

  return (
    <>
      {datasets && (
        <div>
          <h2 className="text-4xl font-extrabold">My Data</h2>
          <p className="mb-4 text-lg font-normal text-gray-400">
            My uploaded data
          </p>
          <Button className="mb-4" onClick={() => setOpenModal(true)}>
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-6 h-6">
              <path strokeLinecap="round" strokeLinejoin="round" d="M12 16.5V9.75m0 0 3 3m-3-3-3 3M6.75 19.5a4.5 4.5 0 0 1-1.41-8.775 5.25 5.25 0 0 1 10.233-2.33 3 3 0 0 1 3.758 3.848A3.752 3.752 0 0 1 18 19.5H6.75Z" />
            </svg>
            Upload a file
          </Button>
          <Modal show={openModal} onClose={() => setOpenModal(false)}>
            <Modal.Header>
              <div className="flex flex-row items-center">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-6 h-6">
                  <path strokeLinecap="round" strokeLinejoin="round" d="M12 16.5V9.75m0 0 3 3m-3-3-3 3M6.75 19.5a4.5 4.5 0 0 1-1.41-8.775 5.25 5.25 0 0 1 10.233-2.33 3 3 0 0 1 3.758 3.848A3.752 3.752 0 0 1 18 19.5H6.75Z" />
                </svg>
                <p className="ml-2">Upload a File</p>
              </div>
            </Modal.Header>
            <Modal.Body>
              <div className="space-y-6">
                <div>
                  <div className="mb-2 block">
                    <Label htmlFor="file-upload" value="Upload file" />
                  </div>
                  <FileInput id="file-upload" onChange={handleFileChange} />
                </div>

              </div>
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={handleSubmit} disabled={isLoading}>
                {isLoading ? "Uploading..." : "Upload"}
              </Button>
              <Button color="gray" onClick={() => setOpenModal(false)}>
                Cancel
              </Button>
            </Modal.Footer>
          </Modal>
          {/* <Modal isOpen={modalIsOpen} onRequestClose={closeModal}>
          <h2>Select dataset to upload</h2>
          <input type="file" onChange={handleFileChange} />
          <button onClick={handleSubmit} disabled={isLoading} className="upload-button">
            {isLoading ? "Uploading..." : "Upload"}
          </button>
        </Modal> */}
          <div className="w-full mb-12 xl:mb-0 mx-auto mt-2">
            <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded ">
              <div className="rounded-t mb-0 px-4 py-3 border-0">
                <div className="flex flex-wrap items-center">
                  <div className="relative w-full px-4 max-w-full flex-grow flex-1">
                    <h3 className="font-semibold text-base text-blueGray-700">User Data</h3>
                  </div>
                </div>
              </div>

              <div className="block w-full overflow-x-auto">
                <table className="items-center bg-transparent w-full border-collapse ">
                  <thead>
                    <tr>
                      <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                        File Name
                      </th>
                      <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                      </th>
                    </tr>
                  </thead>

                  <tbody>
                    {datasets.map((dataset, index) => {
                      return (
                        <tr key={index}>
                          <th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left text-blueGray-700 ">
                            {dataset.filename}
                          </th>
                          <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 ">
                            <button type="button" onClick={() => handleDownload(dataset.filename)} className="px-3 py-2 text-xs font-medium text-center inline-flex items-center focus:outline-none rounded-lg border hover:bg-gray-100 focus:z-10 focus:ring-4 focus:ring-gray-700 bg-gray-800 text-gray-400 border-gray-600 hover:text-white hover:bg-gray-700">
                              <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round" d="m9 13.5 3 3m0 0 3-3m-3 3v-6m1.06-4.19-2.12-2.12a1.5 1.5 0 0 0-1.061-.44H4.5A2.25 2.25 0 0 0 2.25 6v12a2.25 2.25 0 0 0 2.25 2.25h15A2.25 2.25 0 0 0 21.75 18V9a2.25 2.25 0 0 0-2.25-2.25h-5.379a1.5 1.5 0 0 1-1.06-.44Z" />
                              </svg>
                              Download
                            </button>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default ModelSummary;
