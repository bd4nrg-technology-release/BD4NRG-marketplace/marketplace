'use client'
import { useEffect, useState } from 'react';

export default function Table() {
  const [username, setUsername] = useState('');

  useEffect(() => {
    if (typeof window !== 'undefined') {
      // This code runs only on the client side
      const storedUsername = sessionStorage.getItem('user');
      if (storedUsername) {
        setUsername(storedUsername);
      }
    }
  }, []);


  return (
    <table className="table-auto">
      <thead>
        <tr>
          <th>Name</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>{username}</td>
        </tr>
      </tbody>
    </table>
  );
}
