'use client'
import React, { useState, useEffect } from "react";
import TransactionsTable from './components/TransactionsTable.js'; // Assuming you have a component to display transactions
import NotLoggedInPage from "../common/login/page.js";


function UserInfo() {
  const [token, setToken] = useState('');
  const [transactions, setTransactions] = useState([]);

// There is no env, lets use this at least
  // HTTPS server
  const rootUrlServices = 'https://api.marketplace.bd4nrg.eu'
  // Local deploy
  // const rootUrlServices = 'http://localhost:5080'

  useEffect(() => {
    if (typeof window !== 'undefined') {
      const storedToken = sessionStorage.getItem('access_token');
      if (storedToken) {
        setToken(storedToken);
        fetchTransactions(storedToken);
      }
    }
  }, []);

  const fetchTransactions = async (auth_token) => {
    const requestOptions = {
      method: 'GET'
    };
    try {
      const response = await fetch(rootUrlServices + '/transactions?auth_token=' + auth_token, requestOptions);
      if (!response.ok) throw new Error('Error fetching transactions');
      const data = await response.json();
      console.log(data); // Debugging log
      setTransactions(data);
    } catch (error) {
      console.error('Failed to fetch transactions:', error);
    }
  };

  if (!token) {
    return <NotLoggedInPage />;
  }

  return (
    <div>
      <h2 className="text-4xl font-extrabold">User Info</h2>
      <p className="mb-4 text-lg font-normal text-gray-400">
        User History
      </p>
      <div>
        <TransactionsTable transactions={transactions} />
      </div>
      <br />
    </div>
  );
};

export default UserInfo;
