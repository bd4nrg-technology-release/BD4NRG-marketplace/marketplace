import React from "react";
import useDownloader from "react-use-downloader";
  
export default function Downloader() {
  const { size, elapsed, percentage, download,
        cancel, error, isInProgress } =
    useDownloader();
  
  const fileUrl = "/Dataset_X.xlsx";
  const filename = "Dataset_X.xlsx";
  
  return (
    <div className="Downloader">
      <h3>BD4NRG - File Downloader</h3>
      <p>Download is in {isInProgress ? 
        "in progress" : "stopped"}</p>
  
      <button onClick={() => download(fileUrl, filename)}>
        Click to download the file
      </button>
      <button onClick={() => cancel()}>
        Cancel the download
      </button>
      <p>Download size in bytes {size}</p>
  
      <label for="file">Downloading progress:</label>
      <progress id="file" value={percentage} max="100" />
      <p>Elapsed time in seconds {elapsed}</p>
      {error && <p>possible error {JSON.stringify(error)}</p>}
    </div>
  );
}