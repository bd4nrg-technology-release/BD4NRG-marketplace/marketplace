export default function Table () {

return <table className="table p-4 bg-white rounded-lg shadow">
    <thead>
        <tr>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
                Dataset Name
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
                Type
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
                Format
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
                Metadata
            </th>
            <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
                Reputation
            </th>
        </tr>
    </thead>
    <tbody>
        <tr className="text-gray-700">
            <td className="border-b-2 p-4 dark:border-dark-5">
                Dataset from WP3 bucket
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
                Social information
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
                XLSX
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
                Origin, Date
            </td>
            <td className="border-b-2 p-4 dark:border-dark-5">
                *****
            </td>
        </tr>
    </tbody>
</table>

}