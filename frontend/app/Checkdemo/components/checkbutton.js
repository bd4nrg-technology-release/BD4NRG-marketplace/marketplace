import Link from 'next/link';

export default function CheckButton () {
    return ( 
/*     <div align="center"><Link href='/Checkdemo/EAIresults' className='text-white rounded shadow bg-projectBlue-300 p32 hover:bg-projectPink-50'>Test BD4NRG dataset</Link></div>
 */    <button type="button" className="py-2 px-4 flex justify-center items-center  bg-projectBlue-600 hover:bg-projectPink-500 focus:ring-red-500 focus:ring-offset-red-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">
    <Link href='/Checkdemo/EAIresults' className='text-white'>Test BD4NRG dataset</Link>
    </button>
    )
}