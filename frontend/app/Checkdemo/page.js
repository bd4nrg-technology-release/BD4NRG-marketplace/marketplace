
import React from "react";
import TestButton from './components/button.js';
import CheckButton from './components/checkbutton.js';
import Select from './components/select.js';
import Table from './components/table.js';
import Downloader from './components/download.js';
import Link from 'next/link';
  
export function Checkdemo () {
  return (
    <><><><div>
      <h1>Look below for a snippet of what can BD4NRG do for you</h1>
    </div><br /><h2>Start by selecting one of the datasets apt to test</h2><br />
      <Select /><br /><h2>Which are the details of such dataset?</h2><br /><Table /><br />
      <br /></></><div style={{
      position: 'absolute', left: '50%', top: '75%',
      transform: 'translate(-50%, -50%)'
    }}><CheckButton /></div></>
  );
};
  

export default Checkdemo;

