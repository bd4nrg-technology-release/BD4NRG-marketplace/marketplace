import Image from 'next/image'
import summary from '../../../public/img/summary-plot.png'
import force from '../../../public/img/force-plot.png'
import waterfall from '../../../public/img/waterfall-plot.png'

export default function EAIresults () {
    return (
            <><span className='pl-8 text-2xl text-projectBlack-400'> Explainable AI service analysis graphical results </span><br /><br />

            <ul className="flex flex-wrap items-start gap-8">
                <li className="w-2/5">
                    <Image src={summary} width={550} height='auto' alt='Summary plot' /><span className='pl-8 text-2xl text-white'> Summary plot </span><br /><br />
                </li>
                <li className="w-2/5">
                    <Image src={waterfall} width={550} height='auto' alt='Waterfall plot' /><span className='pl-8 text-2xl text-white'> Waterfall plot </span>
                </li>
                <li className="w-2/5">
                    <Image src={force} width={850} height='auto' alt='Force plot' /><span className='pl-8 text-2xl text-white'> Force plot </span><br /><br />
                </li>
            </ul></>
    )
}