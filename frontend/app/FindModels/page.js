'use client'
import React, { useEffect, useState } from "react";
import './FindModels.css';

function AllModelSummary () {
  const [datasets, setDatasets] = useState([]);
  const [modalIsOpen, setIsOpen] = useState(false);
  const [selectedFile, setSelectedFile] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [bucketName, setBucketName] = useState('');
  const [selectedDataset, setSelectedDataset] = useState(null);

  const handleRowClick = (dataset) => {
    setSelectedDataset(dataset);
  };

  const handleDownload = async () => {
    if(!selectedDataset) return;

    const authToken = sessionStorage.getItem('access_token');
    // There is no env, lets use this at least
    // HTTPS server
    const rootUrlServices = 'https://api.marketplace.bd4nrg.eu'
    // Local deploy
    // const rootUrlServices = 'http://localhost:5080'

    const request_url = new URL(rootUrlServices + '/mlaas/models/consumer');
    request_url.search = new URLSearchParams({
      bucket_name: selectedDataset.provider_id,
      object_name: selectedDataset.filename,
    });
    
    const response = await fetch(request_url, {
      method: 'GET',
      headers: { 'Authorization': `Bearer ${authToken}` },
    })
    
    const blob = await response.blob();

    const url = window.URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', `${selectedDataset.filename}`);
    document.body.appendChild(link);
    link.click();
  };

  useEffect(() => {
    // Fetch data from the server when the component loads
    const fetchData = async () => {
      const authToken = sessionStorage.getItem('access_token');
      console.log(authToken);
      const response = await fetch(rootUrlServices + '/mlaas/model/list/consumer', {
        headers: { 'Authorization': `Bearer ${authToken}` }
      });
      const data = await response.json();
      console.log(data);
      setDatasets(data);
    };

    fetchData();
  }, []);

  return (
    <>
      {datasets && (
        <div className="dataset-summary">
        <h1><th>Find shared models</th></h1>
          {selectedDataset && (
            <button className="download-button" onClick={handleDownload}>Download</button>
          )}
          <table className="dataset-table">
            <thead>
              <tr>
                <th>Provider ID</th>
                <th>File Name</th>
                <th>Upload Date</th>
                <th>Dataset Type</th>
              </tr>
            </thead>
            <tbody>
              {datasets.map(dataset => (
                  <tr 
                    key={dataset.provider_id} 
                    onClick={() => handleRowClick(dataset)}
                    className={selectedDataset && selectedDataset.filename === dataset.filename ? "selected-row" : ""}
                  >
                  <td>{dataset.provider_id}</td>
                  <td>{dataset.filename}</td>
                  <td>{dataset.upload_date}</td>
                  <td>{dataset.dataset_type}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      )}
    </>
  );
};

export default AllModelSummary;
