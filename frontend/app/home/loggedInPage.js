'use client'
import { signOut } from 'next-auth/react'
import PilotSiteList from './piltoSiteList.js'

export default function LoggedInPage ({ user, roles }) {
  return (
    <>
      <div>
        You are signed in <br />
        <button
          onClick={() => signOut({ callbackUrl: '/api/auth/keycloak-signout' })}
        >
          Sign out
        </button>
      </div>
    </>
  )
}
