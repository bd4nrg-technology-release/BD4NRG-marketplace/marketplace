import Link from 'next/link'
import { pilotSitesRoles } from '../utils/roles-mapping.js'
import { isAdmin } from '../utils/sign-in.js'
export default function PilotSitesList ({ roles }) {
  const actualRoles = isAdmin(roles) ? pilotSitesRoles : roles
  return (
    <>
      {actualRoles.map(item => {
        return (
          <li key={item}>
            <Link href={`/${item}`}>{item}</Link>
          </li>
        )
      })}
    </>
  )
}
