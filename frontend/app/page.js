'use client'
import React, { useState, useEffect } from "react";
import NotLoggedInPage from './common/notLoggedInPage.js'
import UserInfo from './UserInfo/page.js'

export default function Page () {
  const [token, setToken] = useState('');

  useEffect(() => {
    // sessionStorage will now be accessed only on the client-side
    if (typeof window !== 'undefined') {
      const storedToken = sessionStorage.getItem('access_token');
      if (storedToken) {
        setToken(storedToken);
      }
    }
  }, []);

  return (
    <>
      <div className='sm:ml-14'>
        {token ? (
          <UserInfo />
        ) : (
          <NotLoggedInPage />
        )}
      </div>
    </>
  )
}
