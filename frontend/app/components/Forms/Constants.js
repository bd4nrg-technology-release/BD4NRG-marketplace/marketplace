export const PwdRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}/

export const PwdValidationMsg = 'Password must be at least 8 characters, and contain at least one lowercase, uppercase, symbol and number.'

