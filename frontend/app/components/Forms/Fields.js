import { useField } from 'formik'
import { FileInput } from 'flowbite-react'

export function FormInput ({ label, ...props }) {
  const [field, meta] = useField(props)
  return (
    <div className='flex flex-col'>
      {label && (
        <label htmlFor={props.name} className='text-xl text-gray-500'>
          {label}
        </label>
      )}
      <input
        className='px-3 py-3 my-2 bg-white border-2 rounded-lg focus:ring focus:outline-none disabled:opacity-25 border-incitEV-blue'
        {...field}
        {...props}
      />
      {meta.touched && meta.error ? (<div className='text-red-400 max-w-[300px]'>{meta.error}</div>) : null}
    </div>
  )
}

export function FormCheckbox ({ children, ...props }) {
  const [field, meta] = useField(props)
  return (
    <div className='flex flex-col'>
      <label className='text-gray-500'>
        <input
          {...field}
          {...props}
          type='checkbox'
          className='disabled:opacity-25'
        />
        {children}
      </label>
      {meta.touched && meta.error ? (<div className='text-red-400'>{meta.error}</div>) : null}
    </div>
  )
}

export function FormFile ({ ...props }) {
  const [, meta] = useField(props)
  return (
    <div className='flex flex-col'>
      <FileInput
        {...props}
        className='disabled:opacity-25'
      />
      {meta.value && meta.error ? (<div className='text-red-400'>{meta.error}</div>) : null}
    </div>
  )
}

