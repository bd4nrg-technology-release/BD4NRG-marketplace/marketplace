import Image from 'next/image'
import logo from '../../../public/img/ue-icon.png'
import EuAcknowledge from './EuAcknowledge.js'

export default function Footer() {
  return (
    <>
      {/* <div className='mx-auto bg-projectPink-400 top-[100vh] text-projectGrey-400'>
        <h2 className='p-2 text-xl text-center text-white'>Big Data for Next Generation Energy</h2>
        <div>
          <Image src={logo} width={50} height='auto' alt='EU Logo' />
          <div className='pl-2 text-center text-white text-s'>
          <p>BD4NRG project has received funding from the European Union's Horizon 2020 
            research and innovation programme under grant agreement no. 872613.
          </p>
        </div>
        </div>
      </div> */}
      <footer className="fixed w-full bottom-0 sm:ml-64 shadow bg-gray-900">
        <div className="w-full max-w-screen-xl mx-auto p-4 md:py-8">
          <div className="sm:flex sm:items-center sm:justify-between">
            <div className="flex items-center mb-4 sm:mb-0 space-x-3 rtl:space-x-reverse">
              <Image src={logo} width={50} height='auto' alt='EU Logo' />
              <span className="self-center text-2xl font-semibold whitespace-nowrap text-white">Big Data for Next Generation Energy</span>
            </div>
          
          </div>
          <div>
          <div className='text-white italic text-sm'>
              <p>BD4NRG project has received funding from the European Union's Horizon 2020
                research and innovation programme under grant agreement no. 872613.
              </p>
            </div>
          </div>
        </div>
      </footer>
    </>
  )
}


