'use client'
import { Form, Formik } from 'formik'
import * as Yup from 'yup'
import { useState, useEffect } from 'react'
import { FormInput } from '../../components/Forms/Fields'

export default function UserLoginPage() {
  const [error, setError] = useState(null);
  const [email, setEmail] = useState(''); // State to store email
  const [token, setToken] = useState('');

// There is no env, lets use this at least
  // HTTPS server
  const rootUrlServices = 'https://api.marketplace.bd4nrg.eu'
  // Local deploy
  // const rootUrlServices = 'http://localhost:5080'

  useEffect(() => {
    if (typeof window !== 'undefined') {
      const storedToken = sessionStorage.getItem('access_token');
      if (storedToken) {
        setToken(storedToken);
      }
    }
  }, []);

  const acceptTerms = async () => {
    const response = await fetch(rootUrlServices + '/accept_terms', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ username: email }),
    });
    if (response.ok) {
      setError(null);
      alert("Terms accepted successfully.");
    } else {
      alert("Failed to accept terms.");
    }
  };

  if (token) {
    window.location.replace("/UserInfo");
  }

  return (
    <div className='flex flex-col items-center justify-center'>
      <Formik
        initialValues={{
          email: '',
          password: ''
        }}
        validationSchema={Yup.object({
          email: Yup.string()
            .email('Invalid email address')
            .required('Required'),
          password: Yup.string()
            .required('Required')
        })}
        onSubmit={async (values, { setSubmitting }) => {
          setEmail(values.email); // Update email state on submit
          const requestOptions = {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              username: values.email,
              password: values.password,
            }),
          };
          const res = await fetch(rootUrlServices + '/api/login', requestOptions);
          if (res.status === 200) {
            const data = await res.json();
            sessionStorage.setItem('access_token', data.access_token);
            sessionStorage.setItem('user', values.email);
            window.location.replace("/UserInfo");
          } else if (res.status === 403) {
            setError("User hasn't accepted the regulations.");
          } else {
            const errMsg = await res.json().then(msg => JSON.stringify(msg));
            setError(`${res.status}: ${errMsg}`);
          }
          setSubmitting(false);
        }}
      >
        {({ isSubmitting }) => (
          <Form className='flex flex-col items-center justify-center p-10 bg-white rounded shadow-lg'>
            <h2 className='mb-6 text-2xl font-bold text-gray-600'>Log in</h2>
            <div className='grid grid-cols-2 gap-y-2 gap-x-6'>
              <FormInput
                label='Email'
                name='email'
                type='text'
                placeholder='jdoe@email.com'
                required
              />
              <FormInput
                label='Password'
                name='password'
                type='password'
                required
              />
            </div>
            <button
              type='submit'
              className='w-56 px-4 py-2 mx-4 mt-4 text-xl text-white bg-gray-700 border border-gray-700 rounded hover:bg-gray-500'
              disabled={isSubmitting}
            >
              Sign in
            </button>
            {error && (
              <div>
                <div className='p-2 mb-4 text-xl font-bold text-center text-red-400'>
                  {error}
                </div>
                <p className="mb-4 text-gray-600">
                  "Throughout the access to this platform, you will access confidential information from third parties. This confidential information may not be reproduced by any means or in any format.<br/>
                  By accepting terms I commit to keep strict confidentiality and to not divulge to third parties, neither completely nor partially, the information and confidential documentation of which I do not have the ownership."
                </p>
                <button
                  onClick={acceptTerms}
                  className='px-4 py-2 text-white bg-blue-500 border border-blue-500 rounded hover:bg-blue-400'
                >
                  Accept Terms
                </button>
              </div>
            )}
          </Form>
        )}
      </Formik>
    </div>
  )
}
