"use client"

import React, { useState } from 'react';

function DatasetForm(props) {
const [name, setName] = useState('');
const [city, setType] = useState('');

const changeName = (event) => {
	setName(event.target.value);
};

const changeType = (event) => {
	setType(event.target.value);
};

const transferValue = (event) => {
	event.preventDefault();
	const val = {
	name,
	city,
	};
	props.func(val);
	clearState();
};

const clearState = () => {
	setName('');
	setType('');
};

return (
	<div>
	<label>Name</label>
	<input type="text" value={name} onChange={changeName} />
	<label>Type</label>
	<input type="text" value={city} onChange={changeType} />
	<button onClick={transferValue}> Click Me</button>
	</div>
);
}

export default DatasetForm;
