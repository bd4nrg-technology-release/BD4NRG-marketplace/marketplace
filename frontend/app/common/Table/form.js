"use client"

import React, { useState } from 'react';
import DatasetForm from './form';
/* import jsonData from './data.json'; */

function TableData() {
/* const [studentData, setStudentData] = useState(jsonData); */
const [studentData, setStudentData] = useState("5");

const tableRows = studentData.map((info) => {
	return (
	// eslint-disable-next-line react/jsx-key
	<tr>
{/* 		<td>{info.id}</td>
		<td>{info.name}</td>
		<td>{info.type}</td> */}
        <td>Test</td>
        <td>Look</td>
	</tr>
	);
});

const addRows = (data) => {
	const totalStudents = studentData.length;
	data.id = totalStudents + 1;
	const updatedStudentData = [...studentData];
	updatedStudentData.push(data);
	setStudentData(updatedStudentData);
};

return (
	<div>
	<table className="table table-stripped">
		<thead>
		<tr>
			<th>Dataset</th>
			<th>Name</th>
			<th>Type</th>
		</tr>
		</thead>
		<tbody>{tableRows}</tbody>
	</table>
	<DatasetForm func={addRows} />
	</div>
);
}

export default TableData;
