'use client'
import React, { useEffect, useState } from "react";
import { FileInput } from 'flowbite-react'
import Image from 'next/image'
import logo from '../../../public/img/bd4nrg-logo.png'
import Gear from './icons/Gear.js'
import User from './icons/User.js'
import MachineLearning from './icons/MachineLearning.js'
import Data from './icons/Data.js'
import Analysts from './icons/Analysts.js'
import Link from "next/link";

export default function NavBar() {
  const [username, setUsername] = useState(null);

  useEffect(() => {
    // Function to update the username state based on session storage
    const updateUser = () => {
      const storedUser = sessionStorage.getItem('user');
      setUsername(storedUser || null);
    };

    // Update the username at the start
    updateUser();

    // Add event listener for storage changes
    window.addEventListener('storage', updateUser);

    // Cleanup event listener on component unmount
    return () => window.removeEventListener('storage', updateUser);
  }, []);

  const handleLogout = () => {
    // Clear user and access_token from the session storage
    sessionStorage.removeItem('user');
    sessionStorage.removeItem('access_token');

    // Update username state to reflect logout
    setUsername(null);

    // Redirect to home page - assuming you're running your app on localhost port 3000
    window.location.replace("/");
  };

  return (
    // <>
    //   <div className='flex items-center p-2 sm:ml-14 bg-projectPink-400'>
    //       <a>
    //         <h2 className='text-2xl text-white'> BD4NRG </h2>
    //       </a>
    //       {username ? (
    //         <div id='user' className='flex-1 mr-8 text-right'>
    //             <p className='text-white'>Logged in as: {username}</p>
    //             <button onClick={handleLogout} className='text-white'>
    //               Logout
    //             </button>
    //         </div>
    //       ) : (
    //         <div id='user' className='flex-1 mr-8 text-right'>
    //           <p className='text-white'>Not logged in</p>
    //         </div>
    //       )}
    //   </div>
    // </>

    // <nav className="z-10 w-full px-4 py-1 bg-projectPink-400">
    //   <div className="flex flex-wrap justify-between p-4">
    //     {/* this should be a logo or something, not text! */}
    //     <div className="text-[2rem] leading-[3rem] tracking-tight font-bold text-black dark:text-white">
    //       BD4NRG
    //     </div>
    //     {username ? (
    //       <div className="flex items-center space-x-4 text-lg font-semibold tracking-tight">
    //         <button onClick={handleLogout} className="px-6 py-2 text-white transition duration-500 ease-out bg-blue-700 rounded-lg hover:bg-blue-800 hover:ease-in hover:underline">
    //           Log out
    //         </button>
    //       </div>
    //     ) : (
    //     <div className="flex items-center space-x-4 text-lg font-semibold tracking-tight">
    //       <button className="px-6 py-2 text-white transition duration-500 ease-out bg-blue-700 rounded-lg hover:bg-blue-800 hover:ease-in hover:underline">
    //         Log in
    //       </button>
    //     </div>
    //     )}
    //   </div>
    // </nav>

    <>
      <nav className="fixed top-0 z-50 w-full border-b border-gray-200 bg-projectPink-400">
        <div className="px-3 py-3 lg:px-5 lg:pl-3">
          <div className="flex items-center justify-between">
            <div className="flex items-center justify-start rtl:justify-end">
              <Link href="/" className="flex ms-2 md:me-24">
                <Image src={logo} width={36} height='auto' alt='BD4NRG Logo' />
                <span className='pl-8 text-2xl text-white'> BD4NRG </span>
              </Link>
            </div>
            {username ? (
              <div className="flex items-center space-x-4 text-lg font-semibold tracking-tight">
                <button onClick={handleLogout} className="px-6 py-2 text-white transition duration-500 ease-out bg-blue-700 rounded-lg hover:bg-blue-800 hover:ease-in hover:underline">
                  Log out
                </button>
              </div>
            ) : (
              <div className="flex items-center space-x-4 text-lg font-semibold tracking-tight">
                <Link href="/common/login/" passHref>
                  <button className="px-6 py-2 text-white transition duration-500 ease-out bg-blue-700 rounded-lg hover:bg-blue-800 hover:ease-in hover:underline">
                    Log in
                  </button>
                </Link>
              </div>
            )}
          </div>
        </div>
      </nav>

      <aside id="logo-sidebar" className="fixed top-0 left-0 z-40 w-64 h-screen pt-20 transition-transform -translate-x-full border-r sm:translate-x-0 bg-gray-800 border-gray-700" aria-label="Sidebar">
        <div className="h-full px-3 pb-4 overflow-y-auto bg-gray-800">
          <ul className="space-y-2 font-medium">
            <li>
              <Link href="/UserInfo" className="flex items-center p-2 rounded-lg text-white hover:bg-gray-700 group">
                <User className='flex-shrink-0 w-10 h-10 fill-zinc-400 group-hover:fill-white'></User>
                <span className="ms-3 pl-4">User Info</span>
              </Link>
            </li>
            <li>
              <Link href="/MyData" className="flex items-center p-2 rounded-lg text-white hover:bg-gray-700 group">
                <Data className='flex-shrink-0 w-10 h-10 fill-zinc-400 group-hover:fill-white'></Data>
                <span className="ms-3 pl-4">My Data</span>
              </Link>
            </li>
            <li>
              <Link href="/FindData" className="flex items-center p-2 rounded-lg text-white hover:bg-gray-700 group">
                <Data className='flex-shrink-0 w-10 h-10 fill-zinc-400 group-hover:fill-white'></Data>
                <span className="ms-3 pl-4">Find Data</span>
              </Link>
            </li>
            <li>
              <Link href="/AnalyticsServices" className="flex items-center p-2 rounded-lg text-white hover:bg-gray-700 group">
                <Analysts className='flex-shrink-0 w-10 h-10 fill-zinc-400 group-hover:fill-white' />
                <span className="ms-3 pl-4">Analytics Services</span>
              </Link>
            </li>
            <li>
              <Link href="/AnalyticsPage" className="flex items-center p-2 rounded-lg text-white hover:bg-gray-700 group">
                <Analysts className='flex-shrink-0 w-10 h-10 fill-zinc-400 group-hover:fill-white' />
                <span className="ms-3 pl-4">Analytics Page</span>
              </Link>
            </li>
            <li>
              <Link href="/MLaaS" className="flex items-center p-2 rounded-lg text-white hover:bg-gray-700 group">
                <Gear className='flex-shrink-0 w-10 h-10 fill-zinc-400 group-hover:fill-white' />
                <span className="ms-3 pl-4">MLaaS</span>
              </Link>
            </li>
            <li>
              <Link href="/SaaS" className="flex items-center p-2 rounded-lg text-white hover:bg-gray-700 group">
                <MachineLearning className='flex-shrink-0 w-10 h-10 fill-zinc-400 group-hover:fill-white' />
                <span className="ms-3 pl-4">SIMaaS</span>
              </Link>
            </li>
          </ul>
        </div>
      </aside>
    </>
  );
}
