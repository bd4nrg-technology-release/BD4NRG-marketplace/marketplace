'use client'
import { useEffect, useState } from 'react';

const WalletComponent = () => {
    const [userAddress, setUserAddress] = useState('');
    const [isRequesting, setIsRequesting] = useState(false);
    const [isMinting, setIsMinting] = useState(false);
    const [account, setAccount] = useState(null);

    const connectWallet = async () => {
        if (isRequesting) return; // if already requesting, don't proceed
        setIsRequesting(true);
        try {
            const [account] = await ethereum.request({ method: 'eth_requestAccounts' });
            setAccount(account); // assuming 'account' is the user's address
            setUserAddress(account); // set userAddress state
        } catch (error) {
            console.error("Error connecting:", error);
            // handle error, e.g., show error message to user
        } finally {
            setIsRequesting(false);
        }
    };

    const mintTokens = async () => {
        if (isMinting) return;
        setIsMinting(true);

        if (!window.ethereum) {
            console.error("MetaMask is not installed!");
            return;
        }
        try {
            const transactionParameters = {
            to: 'RECIPIENT_ADDRESS', // e.g., '0x31BB711de2e457066c6281f231fb473FC5c2afd3'
            from: ethereum.selectedAddress, // the address of the user in MetaMask
            value: '0x29a2241af62c0000', // 0.1 ETH in hexadecimal, in wei
            gas: '0x5208', // 21000 in hexadecimal (the gas limit for standard ETH transfers)
            // You should let the wallet automatically set the gas price or gas fee depending on the EIP-1559 compatibility
            };

          // Now, we're going to send the transaction.
            const txHash = await ethereum.request({
            method: 'eth_sendTransaction',
            params: [transactionParameters],
            });
            console.log("Transaction sent with hash:", txHash);
          // You might want to inform the user that the transaction is in progress and provide the transaction hash so they can track it
        } catch (error) {
            console.error("Error sending transaction:", error);
          // There might be an error for various reasons: rejected by the user, insufficient funds, etc.
        } finally {
            setIsMinting(false);
        }
    };

    // Removed the useEffect that was here as we no longer want to connect automatically

    return (
        <div>
            {userAddress ? (
                <p>Connected Ethereum address: {userAddress}</p>
            ) : (
                <div>
                    <p>Not connected to an Ethereum wallet.</p>
                    <button onClick={connectWallet} disabled={isRequesting}>
                        {isRequesting ? "Connecting..." : "Connect"}
                    </button>
                    <button onClick={mintTokens} disabled={isMinting}>
                        {isMinting ? "Minting..." : "Mminted"}
                    </button>
                </div>
            )}
        </div>
    );
};

export default WalletComponent;

