const ethers = require('ethers');

export const loadBlockchainData = async () => {
    try {
      // Check if MetaMask is installed
      if (!window.ethereum) {
          alert('Please install MetaMask to use this dApp!');
          return;
      }

      // Connect to the user's wallet
      await window.ethereum.request({ method: 'eth_requestAccounts' });

      // We're using the ethers.js library to create a new provider using MetaMask's provider.
      const provider = new ethers.providers.Web3Provider(window.ethereum);

      // We're using the provider to get the signer, i.e., the user's wallet.
      const signer = provider.getSigner();

      // Get the user's Ethereum address
      const address = await signer.getAddress();

      return { provider, signer, address };
    } catch (error) {
        console.error(error);
        alert('Failed to load blockchain data. Check console for details.');
    }
};


