import Link from 'next/link'
export default function SideBarItem ({ text, Item }) {
  return (
    <Link href={`/${text.replace(/ /g, '')}`}>
      <div className='flex items-center pt-1 pb-1 transition-all min-w-max to-white hover:bg-gradient-to-r hover:scale-105 duration-400 via-projectPink-300 from-projectPink-400 group fill-zinc-400'>
        <li className='flex items-center mt-4 mb-4 min-w-max'>
          <Item className='w-10 h-10 fill-zinc-400 group-hover:fill-white' />
          <span className='ml-4 font-medium text-white'>{text}</span>
        </li>
      </div>
    </Link>
  )
}
