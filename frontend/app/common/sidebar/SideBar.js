import Link from 'next/link'
import Image from 'next/image'
import logo from '../../../public/img/bd4nrg-logo.png'
import SideBarItem from './SideBarItem.js'
import Gear from './icons/Gear.js'
import User from './icons/User.js'
import MachineLearning from './icons/MachineLearning.js'
import Privacy from './icons/Privacy.js'
import Data from './icons/Data.js'
import Alarm from './icons/Alarm.js'
import Monitor from './icons/Monitor.js'
import Analysts from './icons/Analysts.js'


export default function SideBar () {
  return (
    <div className='fixed top-0 left-0 z-10 bg-projectBlue-400'>
      <div className='hidden overflow-hidden transition-all ease-in-out border-r w-14 sm:inline-block hover:w-56 hover:shadow-lg xs:hidden duration-400 sidebar border-projectGrey-400 hover:border-projectGrey-400 hover:shadow-projectGrey-400 hover:bg-projectGrey-400'>
        <div className='flex flex-col justify-between min-h-screen pt-2 pb-2 pl-2 tracking-wide'>
          <div className='flex items-center w-max'>
            <Image src={logo} width={36} height='auto' alt='BD4NRG Logo' />
            <span className='pl-8 text-2xl text-white'> BD4NRG </span>
          </div>
          <ul className='mt-6 space-y-2 tracking-wide'>
            <SideBarItem text='User Info' Item={User} />
            <SideBarItem text='Find Data' Item={Data} />
            <SideBarItem text='My Data' Item={Data} />
            <SideBarItem text='Analytics Services' Item={Analysts} />
            <SideBarItem text='Analytics Page' Item={Analysts} />
            <SideBarItem text='MLaaS' Item={Gear} />
            <SideBarItem text='SaaS' Item={MachineLearning} />
          </ul>
          <Link href='/Settings/FAQ'>
            <div className='flex items-center pt-1 pb-1 transition-all min-w-max to-white hover:bg-gradient-to-r hover:scale-105 duration-400 via-projectPink-400 from-projectPink-400 group fill-zinc-400'>
              <Gear className='ml-1 w-7 h-7 fill-zinc-400 group-hover:fill-white' />
              <span className='pl-4 group-hover:text-white text-zinc-400'>
                FAQs
              </span>
            </div>
          </Link>
        </div>
      </div>
    </div>
  )
}
