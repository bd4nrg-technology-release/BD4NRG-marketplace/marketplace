import Link from 'next/link'

export default function NotRightRolesPage ({ user }) {
  return (
    <>
      <div>
        User {user} has no access to this page.
        <br />
        <Link href='/'>Home</Link>
      </div>
    </>
  )
}
