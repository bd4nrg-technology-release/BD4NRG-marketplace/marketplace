"use client"
/* import {React, useState} from 'react';

import { format } from 'date-fns';
import { DayPicker } from 'react-day-picker';

export default function Example() {
   const [selected, setSelected] = useState(new Date('2023-03-01'))

   let footer = <p>Please pick a day.</p>;
   if (selected) {
     footer = <p>You picked {format(selected, 'PP')}.</p>;
   }
   return (
     <DayPicker
       mode="single"
       selected={selected}
       onSelect={setSelected}
       footer={footer}
       numberOfMonths={2}
       pagedNavigation
       showOutsideDays
       fixedWeeks
       showWeekNumber
     />
   );
 }   */

import React, { useState } from "react";
// import "react-modern-calendar-datepicker/lib/DatePicker.css";
// import { Calendar } from "react-modern-calendar-datepicker";

const App = () => {
  const defaultFrom = {
    year: 2023,
    month: 3,
    day: 1,
  };
  const defaultTo = {
    year: 2023,
    month: 3,
    day: 4,
  };
  const defaultValue = {
    from: defaultFrom,
    to: defaultTo,
  };
  const [selectedDayRange, setSelectedDayRange] = useState(
    defaultValue
  );

  return (
    <h1>hello world</h1>
    // <Calendar
    //   value={selectedDayRange}
    //   onChange={setSelectedDayRange}
    //   colorPrimary="#0fbcf9" // added this
    //   colorPrimaryLight="rgba(75, 207, 250, 0.4)" // and this
    //   shouldHighlightWeekends
    // />
  );
};

export default App;