'use client'
import React, { useEffect, useState } from "react";
import "./analytics.css";
import NotLoggedInPage from "../common/login/page.js";

function AT() {
  const [token, setToken] = useState('');

  useEffect(() => {
    // sessionStorage will now be accessed only on the client-side
    if (typeof window !== 'undefined') {
      const storedToken = sessionStorage.getItem('access_token');
      if (storedToken) {
        setToken(storedToken);
      }
    }
  }, []);

  if (!token) {
    return <NotLoggedInPage />;
  }

  return (
    <div>
      <h2 className="text-4xl font-extrabold">Analytics Toolbox</h2>
      <p className="mb-4 text-lg font-normal text-gray-400">
        BD4NRG Analytics Toolbox
      </p>
      <iframe src="https://analyticstoolbox.bd4nrg.eu/" title="AT" className="w-full aspect-video"></iframe>
    </div>
  );
}

export default AT;