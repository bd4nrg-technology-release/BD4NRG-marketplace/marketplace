'use client'
import React, { useEffect, useState, useRef } from "react";
import './FindData.css';
import NotLoggedInPage from "../common/login/page.js";
import { Button } from 'flowbite-react';

function AllDatasetSummary() {
  const [datasets, setDatasets] = useState([]);
  const [selectedDataset, setSelectedDataset] = useState(null);
  const [downloadUrl, setDownloadUrl] = useState(null);

  const downloadRef = useRef();
  const [token, setToken] = useState('');

  // There is no env, lets use this at least
  // HTTPS server
  const rootUrlServices = 'https://api.marketplace.bd4nrg.eu'
  // Local deploy
  // const rootUrlServices = 'http://localhost:5080'

  const assets_conf = {
    "info": "sells_information",
    "sells": [
      {
        "data": {
          "asset_id": "asset_0",
          "asset_sell_info": {
            "signature": "0x496c6e4d0ea32a73e03ffd022263718029cbe529dbef6746b830903bfadd22202b5fc189b7542dd360434cc760930eedf0a84dcf5a9e9fc930d704ba306e7bc01c",
            "hashedRefWithNonce": "0x1c14a722a9290d741057dc4ae05e1b6fb3f47ecd96e9c5a276c158164745b26b",
            "receiptID": "0x90e0c9574cadde53cd28d73f7ed9977f04df4b3ac5c278614cb1a21980549e8f",
            "price": 10
          }
        }
      },
      {
        "data": {
          "asset_id": "asset_1",
          "asset_sell_info": {
            "signature": "0xcd51b064081b28852611d6aad489949d2975eaf4eed05bfd645ad76dcc39574e5df64b19afb143f8a6793381471ef9a86043f4bbd96a58d6082ca3a3d84dc9fa1b",
            "hashedRefWithNonce": "0x6b4ddb28c6121e54c6104df3eb4e71da81c15c2d561fe24fb5ff850d0bb9afea",
            "receiptID": "0xc24d3561ed51e16670a0ff31e7ecdaa172d7b1b984b6664df8b536bf0854567a",
            "price": 10
          }
        }
      },
      {
        "data": {
          "asset_id": "asset_2",
          "asset_sell_info": {
            "signature": "0xbce52737a458cef3fd5f8438884c2d442e1a9789914a24a3419f5482d1e8e2427fd53d81dac52d598b6ae4d5c6f3d637dd50c02bf2634bef277fca4aa3498ed71b",
            "hashedRefWithNonce": "0x5c6a1f3c8aeb049bb4c4e7dfda9645b1affc16b52dead0bbfc2cdfd6cb4d3929",
            "receiptID": "0xe088068896fc90b59f26c4eda6990bb647b567e3d24761158ef7d8d5417ce6b9",
            "price": 10
          }
        }
      },
      {
        "data": {
          "asset_id": "asset_3",
          "asset_sell_info": {
            "signature": "0xce8551452ec1405a03eb2ade96608fdfd971b12c19e0ba0eb9540d4bb8b857463c78152432cd0e80906783549de3052b1ee1b7fcc01b1f0a676047624997fd561c",
            "hashedRefWithNonce": "0xaf19a97a9c7c9b889010138a6df5178b260a9e1dedd7378a2dfbc9847df59cd9",
            "receiptID": "0x2fca9bc8433231e6454591b273071e49986acd35d0275458b8f4f00869ad7489",
            "price": 10
          }
        }
      },
      {
        "data": {
          "asset_id": "asset_4",
          "asset_sell_info": {
            "signature": "0xd098486b5909c4f49672b7ea081461a51903bdb37a866a703f57d52b5889026a7c3bdca36e5540d7285ddd9f05edc570269abd15e273380b71d4af6171ac8d9b1b",
            "hashedRefWithNonce": "0x898d98e59f34bcf8c32adf51e4e9478a6363a59af2c38ed0f9371bf1eab875e0",
            "receiptID": "0xbd8e18b9957d0ab55f20ac43bcc7cd31d4aa62eb432d1a9d16c55d1faba7c3fd",
            "price": 10
          }
        }
      },
      {
        "data": {
          "asset_id": "asset_5",
          "asset_sell_info": {
            "signature": "0x0b1073b3a78e20df1394d25ae0886ec518efdbd1a829c23e93770ac74be2be2b0ad57a2f01a8f4d40b390e252dabc66de413906cf5c9cc4af1abdffe6d9870ad1c",
            "hashedRefWithNonce": "0x83d01fa13f8703735e25b55cb1e099e5158d26042046b4990a2b68bdf2de4768",
            "receiptID": "0x6fd103b180c82890ead8c947e63fb85038ca490db278a3739ff6ea165ba9f9f9",
            "price": 10
          }
        }
      },
      {
        "data": {
          "asset_id": "asset_6",
          "asset_sell_info": {
            "signature": "0x60a01d16e7bc40d5e55053673b6905e36ad249ba578b8c921117ae56221eda8b2c6599a3737d6e2ca85ec4e98a92d58d3bab8c7f850244638a55e9d1efc3c9b61c",
            "hashedRefWithNonce": "0x59aaec00ac35cd1492492769f72e3c9d0736b57e716b2bc3183157dfa02894e0",
            "receiptID": "0x61a8fddabf83ac87b18dd38007d4ecfb020cb65a6f9da09409784813d9dd4b8e",
            "price": 10
          }
        }
      },
      {
        "data": {
          "asset_id": "asset_7",
          "asset_sell_info": {
            "signature": "0x6f61a8917013f8794aaef73be0c1916d81f1b64979cf3bcde4b4ae45994a1a9f66d28502744e5da84377ddc4d0215b4038c897376e7933044c934cbb5cde42631b",
            "hashedRefWithNonce": "0x47bb3a704e94659342734e2210f3298fc61eb256fed571fff59f7fd39a1f48c0",
            "receiptID": "0xc433bb0fac56fd8943f462a2d035f7cd953e20f1b7a3a66205c06f12d42b713e",
            "price": 10
          }
        }
      },
      {
        "data": {
          "asset_id": "asset_8",
          "asset_sell_info": {
            "signature": "0xa6faf1ec264a5f09cccb89b17ffca7bdf3c9c626201c03b8c2e5c59a500ff7167d27a38d4283861bcb5e1fea605c389a2f4064c788d8617205b62856cf06b8e51c",
            "hashedRefWithNonce": "0xaed17bca43e17239b19aec64220722275c2b609c078eb9b1acca36315c3a1777",
            "receiptID": "0xefc955aba152b1ceee5413709c1297298ad5492c297d0a96a03c8f7ee45856db",
            "price": 10
          }
        }
      },
      {
        "data": {
          "asset_id": "asset_9",
          "asset_sell_info": {
            "signature": "0x65f633c5ef3f84fbbc76af582a7295b709ea796c3c54d098829319c306bd067801941608bf8d1bf0f574b92bd322a4cce411c4dfdabca282365fe1ae055abebb1b",
            "hashedRefWithNonce": "0x6a3dec48812389768193e56a7d5d8abb7258dc2c1e0a561e6ef30360bbf01464",
            "receiptID": "0xa989dec8552e55ba73d05fdea75238ec8525662287b66e9048e44d729d96318a",
            "price": 10
          }
        }
      },
      {
        "data": {
          "asset_id": "asset_10",
          "asset_sell_info": {
            "signature": "0x2f4e3c7ca677674bf41c3dc69091ef0804ba86f30ab2ea187e77ce997a251f8c2ba2b69f75199f29798baefde84e96be4af4fd31a9bedaea9f9d0bf2b9047c4e1c",
            "hashedRefWithNonce": "0x587b5f79fabaf557e6a6aace49545102cf6a8e2dcd669ab86153be1b2b2950b1",
            "receiptID": "0xd21a050110db6e096014a29fe5e068664e7d52d2f9bbff17461e98b6356064ff",
            "price": 10
          }
        }
      },
      {
        "data": {
          "asset_id": "asset_11",
          "asset_sell_info": {
            "signature": "0x4318494294d24cc068edb07ad9f7387dd43b4a8d6e04b0a40fe441d0e162311d13efa284980231a717414986123fd9fc2a6e1e3c2a9f9a5da08c7e77ace891501c",
            "hashedRefWithNonce": "0xe04ce4cb863dd2a6f7fb69d065167b47a7dbc76b448a4aa9de8aa4eb622a45ab",
            "receiptID": "0x5bd925ff20c03d26df608ecb3f4c0c262a21ebb28c0ceab3855d028481b7f56a",
            "price": 10
          }
        }
      },
      {
        "data": {
          "asset_id": "asset_12",
          "asset_sell_info": {
            "signature": "0xbdb1fb49eebae1eea7c840bf25e9b956cda7364af4340cd9a001f0a7568a5c775293af79fda5e9838776e179d72d07e41ea997196113d8ef2b1099e9864d4d351c",
            "hashedRefWithNonce": "0x41fd38bf8aeb3e0b1bfce696f1d5e23bd6e029d208a1ae55dc13e4df7b2c6b76",
            "receiptID": "0xcaf59c91b1d93dbcaa3e96c266751e2552a67b3d674e7313ede6a20ffec1e315",
            "price": 10
          }
        }
      },
      {
        "data": {
          "asset_id": "asset_13",
          "asset_sell_info": {
            "signature": "0xb9579048369c46dd6c1fc9a7985a09aef42eb5df53b9e6c15734b65f1793a5062f8c76f432a8c2d04adbda082513e9c8ef5c906609460301bf6101559cc116f11c",
            "hashedRefWithNonce": "0x2bfc41ee91dc991566a8ba758c787f19a2f57ced0a9c4b0c54624d48201fad91",
            "receiptID": "0x4ef399abeb31984090ee548c5a61b25150acc5c41028966622072659f2c75e86",
            "price": 10
          }
        }
      },
      {
        "data": {
          "asset_id": "asset_14",
          "asset_sell_info": {
            "signature": "0x49824eaa9ca8b0fd954f204a6dd83a0ed02caf6336b446c17fb40a953de29aa93f5a2f31abf15b812c90e998b98c4685adb4e66df80bdc08776fffa5f0395b531b",
            "hashedRefWithNonce": "0x4e440221608339ffc6e9d706ec6924ec3955773fe92ce6d6fd0618dbcf2a47c6",
            "receiptID": "0x6b28a5fb465dbc411ac5c5f767f2a9f5956ee98036849608128e5bc5c1df9f9e",
            "price": 10
          }
        }
      },
      {
        "data": {
          "asset_id": "asset_15",
          "asset_sell_info": {
            "signature": "0x63d1af633c62151e49390b0b7808dd2499c44b09d0ec16a2351dbe977caa8f966f2edce7e8b5f0f36af235dde3f2d799abcd6c116a488eb415f8cf9197c8af1b1b",
            "hashedRefWithNonce": "0x952259dd488ea2d8abc76379ca8df04ddaa4e5150c3d0e0508876353842d85f6",
            "receiptID": "0x24fb69f2fe8f4385eb9c5c8e9d8960975623baceb57cee51d006b0ec6eb5d13c",
            "price": 10
          }
        }
      },
      {
        "data": {
          "asset_id": "asset_16",
          "asset_sell_info": {
            "signature": "0x90f6eace11e51b9fc7b500e80d6bc37e5c884fbaed7313602a0bcedc200d20d16edfef7efcee5ed71b2b5544302e63c3498a029b6374f945394a5742883205981c",
            "hashedRefWithNonce": "0x6e84158f76e60cc9a94627fa9eeed0a72b4e82f8d5dc76891d878bb3214d9464",
            "receiptID": "0x295102f6c48c675e20c1bdd908956401fe4debf3cbaf3abfa7185828b833a228",
            "price": 10
          }
        }
      },
      {
        "data": {
          "asset_id": "asset_17",
          "asset_sell_info": {
            "signature": "0xf4f2f0ed6caed29f485aa0dbccf1ac20a84090a18a4ded83b75325989610feaf42e19b7be39ba979c31a51d42b35aff312d95d8c7417b3f4a08651873d340ab01c",
            "hashedRefWithNonce": "0x3768fd0e4c3e7e4db601d5d37e4848ef47c4ab2e89840a45977029ef4232d415",
            "receiptID": "0xcd5d1fec584e11af0790d15074c22e98d171cd15c1076132c14e7e984c30447c",
            "price": 10
          }
        }
      },
      {
        "data": {
          "asset_id": "asset_18",
          "asset_sell_info": {
            "signature": "0xed5289a8932238cd5eaaf42abb20d6a75a2968c5f08b2997395df303f933e6053a7f7e7552de8d82a1c691cbb12edb7411a39da16aa4fe0fdcbb07343bb29b921b",
            "hashedRefWithNonce": "0xffa3f56e62821083738e0d2339cc30fc8ec65c25820c95003a89428028cf7408",
            "receiptID": "0x058edc490e1036b8b36604d43c68ed1df7c355417aaaf514e55a8345970181f7",
            "price": 10
          }
        }
      },
      {
        "data": {
          "asset_id": "asset_19",
          "asset_sell_info": {
            "signature": "0x62b9b8ec9653b328a7ee7ddce253e621e4f86be58c23eec7b3e346107e22029900a19d097f987be2f4d0eb7350b2d33229fa937f73ea973da04400b5a4cd5d3e1c",
            "hashedRefWithNonce": "0x000e03baca8df5e1104a64646c84ff7909b77ca9fd650dcdaa7a72604498a447",
            "receiptID": "0x93c57b82d9636d875d056b21adc6afe8a67b7723c9fe84112114aea81de60bf1",
            "price": 10
          }
        }
      }
    ]
  }


  useEffect(() => {
    // sessionStorage will now be accessed only on the client-side
    if (typeof window !== 'undefined') {
      const storedToken = sessionStorage.getItem('access_token');
      if (storedToken) {
        setToken(storedToken);
      }
    }
  }, []);

  const handleRowClick = (dataset) => {
    setSelectedDataset(dataset);
  };

  const handleDownload = async (dataset) => {
    const auth_token = sessionStorage.getItem('access_token');
    // const request_url = new URL(`https://api.marketplace.bd4nrg.eu/das/data/official/download`);
    const request_url = new URL(rootUrlServices + '/das/data/official/download');
    request_url.search = new URLSearchParams({
      auth_token: auth_token,
    });

    const response = await fetch(request_url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        object_name: dataset,
      }),
    });

    const blob = await response.blob();
    const url = window.URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', `${dataset}`);
    document.body.appendChild(link);
    link.click();
  };
  // never do this... demo requirements, 0 quality code
  const handleBuy = async (dataset) => {
    const auth_token = sessionStorage.getItem('access_token');

    // Buy method, check each filename to assign requestOptions
    var signature = ''
    var hashedRefWithNonce = ''
    var receiptID = ''
    var assetName = ''
    var assetId = ''
    var price = ''

    switch (dataset) {
      case 'ADS_DEMO_DATA (1).csv':
        signature = assets_conf.sells[0].data.asset_sell_info.signature
        hashedRefWithNonce = assets_conf.sells[0].data.asset_sell_info.hashedRefWithNonce
        receiptID = assets_conf.sells[0].data.asset_sell_info.receiptID
        // Maybe the extension could/needs to be removed?
        // assetName = 'ADS_DEMO_DATA (1).csv'
        assetId = assets_conf.sells[0].data.asset_id
        price = assets_conf.sells[0].data.asset_sell_info.price

        break;
      case 'AUGS_DEMO_IMAGE (1).png':
        signature = assets_conf.sells[1].data.asset_sell_info.signature
        hashedRefWithNonce = assets_conf.sells[1].data.asset_sell_info.hashedRefWithNonce
        receiptID = assets_conf.sells[1].data.asset_sell_info.receiptID
        assetId = assets_conf.sells[1].data.asset_id
        price = assets_conf.sells[1].data.asset_sell_info.price

        break;
      case 'AUGS_DEMO_IMAGE.tif':
        signature = assets_conf.sells[2].data.asset_sell_info.signature
        hashedRefWithNonce = assets_conf.sells[2].data.asset_sell_info.hashedRefWithNonce
        receiptID = assets_conf.sells[2].data.asset_sell_info.receiptID
        assetId = assets_conf.sells[2].data.asset_id
        price = assets_conf.sells[2].data.asset_sell_info.price

        break;
      case 'Centrica Photovoltaic Data.zip':
        signature = assets_conf.sells[3].data.asset_sell_info.signature
        hashedRefWithNonce = assets_conf.sells[3].data.asset_sell_info.hashedRefWithNonce
        receiptID = assets_conf.sells[3].data.asset_sell_info.receiptID
        assetId = assets_conf.sells[3].data.asset_id
        price = assets_conf.sells[3].data.asset_sell_info.price

        break;
      case 'DILIGENT AI Asset for the BD4NRG Marketplace.zip':
        signature = assets_conf.sells[4].data.asset_sell_info.signature
        hashedRefWithNonce = assets_conf.sells[4].data.asset_sell_info.hashedRefWithNonce
        receiptID = assets_conf.sells[4].data.asset_sell_info.receiptID
        assetId = assets_conf.sells[4].data.asset_id
        price = assets_conf.sells[4].data.asset_sell_info.price

        break;
      case 'EAIS_DEMO_DATA (1).csv':
        signature = assets_conf.sells[5].data.asset_sell_info.signature
        hashedRefWithNonce = assets_conf.sells[5].data.asset_sell_info.hashedRefWithNonce
        receiptID = assets_conf.sells[5].data.asset_sell_info.receiptID
        assetId = assets_conf.sells[5].data.asset_id
        price = assets_conf.sells[5].data.asset_sell_info.price

        break;
      case 'Veolia-Administrative.zip':
        signature = assets_conf.sells[6].data.asset_sell_info.signature
        hashedRefWithNonce = assets_conf.sells[6].data.asset_sell_info.hashedRefWithNonce
        receiptID = assets_conf.sells[6].data.asset_sell_info.receiptID
        assetId = assets_conf.sells[6].data.asset_id
        price = assets_conf.sells[6].data.asset_sell_info.price

        break;
      case 'Veolia-Education.zip':
        signature = assets_conf.sells[7].data.asset_sell_info.signature
        hashedRefWithNonce = assets_conf.sells[7].data.asset_sell_info.hashedRefWithNonce
        receiptID = assets_conf.sells[7].data.asset_sell_info.receiptID
        assetId = assets_conf.sells[7].data.asset_id
        price = assets_conf.sells[7].data.asset_sell_info.price

        break;
      case 'Veolia-HealthCentre.zip':
        signature = assets_conf.sells[8].data.asset_sell_info.signature
        hashedRefWithNonce = assets_conf.sells[8].data.asset_sell_info.hashedRefWithNonce
        receiptID = assets_conf.sells[8].data.asset_sell_info.receiptID
        assetId = assets_conf.sells[8].data.asset_id
        price = assets_conf.sells[8].data.asset_sell_info.price

        break;
      case 'Veolia-Residential.zip':
        signature = assets_conf.sells[9].data.asset_sell_info.signature
        hashedRefWithNonce = assets_conf.sells[9].data.asset_sell_info.hashedRefWithNonce
        receiptID = assets_conf.sells[9].data.asset_sell_info.receiptID
        assetId = assets_conf.sells[9].data.asset_id
        price = assets_conf.sells[9].data.asset_sell_info.price

        break;

      default:
        // maybe do a default in case something strange happens?
        signature = ''
        hashedRefWithNonce = ''
        receiptID = ''
        assetName = ''
        assetId = ''
        price = 0
        break;
    }

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        receipt: {
          signature: signature,
          hashedRefWithNonce: hashedRefWithNonce,
          receiptID: receiptID,
          assetName: dataset,
          assetId: assetId,
        },
        price: price
      })
      // JSON.stringify({
      //   userId: 1,
      //   title: "Fix my bugs",
      //   completed: false
      // })
    }
    console.log(requestOptions)
    try {
      const response = await fetch(rootUrlServices + '/bd4nrg/tp/purchase?auth_token=' + auth_token, requestOptions);
    } catch (error) {
      console.error("Fetch error: ", error);
    }

    // Download the file
    const request_url = new URL(rootUrlServices + '/das/data/official/download');
    request_url.search = new URLSearchParams({
      auth_token: auth_token,
    });

    const response = await fetch(request_url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        object_name: dataset,
      }),
    });

    const blob = await response.blob();
    const url = window.URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', `${dataset}`);
    document.body.appendChild(link);
    link.click();
  }

  useEffect(() => {
    if (downloadUrl) {
      downloadRef.current.click();
      // Clear the URL after triggering to prevent multiple downloads
      setDownloadUrl(null);
    }
  }, [downloadUrl]);

  useEffect(() => {
    const fetchData = async () => {
      const auth_token = sessionStorage.getItem('access_token');
      const requestOptions = {
        method: 'GET'
      };
      const response = await fetch(rootUrlServices + '/das/data/official/list?auth_token=' + auth_token, requestOptions);
      const data = await response.json();
      const formattedData = data.map(filename => ({ filename }));
      setDatasets(formattedData);
    };

    fetchData();
  }, []);

  if (!token) {
    return <NotLoggedInPage />;
  }

  return (
    <>
      {datasets && (
        <div>
          <h2 className="text-4xl font-extrabold">Find Data</h2>
          <p className="mb-4 text-lg font-normal text-gray-400">
            Official Data
          </p>
          {/* yeah yeah i know that a a cant wrap a button, gonna go fast */}
          <a target="_blank" href="http://bd4nrg.epu.ntua.gr:8002/" rel="noopener noreferrer">
            <Button className="mb-4">
              <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-6 h-6">
                <path strokeLinecap="round" strokeLinejoin="round" d="M13.19 8.688a4.5 4.5 0 0 1 1.242 7.244l-4.5 4.5a4.5 4.5 0 0 1-6.364-6.364l1.757-1.757m13.35-.622 1.757-1.757a4.5 4.5 0 0 0-6.364-6.364l-4.5 4.5a4.5 4.5 0 0 0 1.242 7.244" />
              </svg>
              Open Query Engine
            </Button>
          </a>
          <div className="w-full mb-12 xl:mb-0 mx-auto mt-2">
            <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded ">
              <div className="block w-full overflow-x-auto">
                <table className="items-center bg-transparent w-full border-collapse ">
                  <thead>
                    <tr>
                      <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                        File Name
                      </th>
                      <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                      </th>
                    </tr>
                  </thead>

                  <tbody>
                    {datasets.map((dataset, index) => {
                      return (
                        <tr key={index}>
                          <th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left text-blueGray-700 ">
                            {dataset.filename}
                          </th>
                          <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 ">
                            {/* <button type="button" onClick={() => handleDownload(dataset.filename)} className="px-1 py-2 text-xs text-white font-medium text-center inline-flex items-center text-gray-900 focus:outline-none rounded-lg border focus:z-10 focus:ring-4 focus:ring-gray-700 bg-gray-800 text-gray-400 border-gray-600 hover:text-white hover:bg-gray-700">
                              <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round" d="m9 13.5 3 3m0 0 3-3m-3 3v-6m1.06-4.19-2.12-2.12a1.5 1.5 0 0 0-1.061-.44H4.5A2.25 2.25 0 0 0 2.25 6v12a2.25 2.25 0 0 0 2.25 2.25h15A2.25 2.25 0 0 0 21.75 18V9a2.25 2.25 0 0 0-2.25-2.25h-5.379a1.5 1.5 0 0 1-1.06-.44Z" />
                              </svg>
                              Download
                            </button> */}
                            <button type="button" onClick={() => handleBuy(dataset.filename)} className="px-1 py-2 text-xs text-white font-medium text-center inline-flex items-center text-gray-900 focus:outline-none rounded-lg border focus:z-10 focus:ring-4 focus:ring-gray-700 bg-gray-800 text-gray-400 border-gray-600 hover:text-white hover:bg-gray-700">
                              <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-6 h-6">
                                <path strokeLinecap="round" strokeLinejoin="round" d="m9 13.5 3 3m0 0 3-3m-3 3v-6m1.06-4.19-2.12-2.12a1.5 1.5 0 0 0-1.061-.44H4.5A2.25 2.25 0 0 0 2.25 6v12a2.25 2.25 0 0 0 2.25 2.25h15A2.25 2.25 0 0 0 21.75 18V9a2.25 2.25 0 0 0-2.25-2.25h-5.379a1.5 1.5 0 0 1-1.06-.44Z" />
                              </svg>
                              Acquire for 10 tokens
                            </button>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default AllDatasetSummary;