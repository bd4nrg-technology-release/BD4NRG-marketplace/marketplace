import Link from 'next/link';

export default function FAQ () {
    
return <div className="max-w-screen-xl p-8 mx-auto">
    <h2 className="mb-12 text-3xl font-extrabold leading-9 text-projectBlack-400 border-b-2 border-gray-100">
        BD4NRG FAQs
    </h2>
    <ul className="flex flex-wrap items-start gap-8">
        <li className="w-2/5">
            <p className="text-lg font-medium leading-6 text-projectBlack-400">
                What does BD4NRG do?
            </p>
            <p className="mt-2">
                <p className="text-base leading-6 text-projectBlack-400">
                Establishing new market opportunities in energy sector by unlocking and exploiting the potential of big data to enable improved operation for all stakeholders in the energy value chain.
                </p>
            </p>
        </li>
        <li className="w-2/5">
            <p className="text-lg font-medium leading-6 text-projectBlack-400">
                Which is the aim of BD4NRG framework?
            </p>
            <p className="mt-2">
                <p className="text-base leading-6 text-projectBlack-400">
                    Going towards techno-economic optimal management of Electric Power and Energy Systems.
                </p>
            </p>
        </li>
        <li className="w-2/5">
            <p className="text-lg font-medium leading-6 text-projectBlack-400">
                How does BD4NRG demonstrate its findings?
            </p>
            <p className="mt-2">
                <p className="text-base leading-6 text-projectBlack-400">
                    BD4NRG framework is demonstrated and validated in real-life pilots in 12 demo-sites across 8 countries.
                </p>
            </p>
        </li>
        <li className="w-2/5">
            <p className="text-lg font-medium leading-6 text-projectBlack-400">
                What comprises such BD4NRG framework?
            </p>
            <p className="mt-2">
                <p className="text-base leading-6 text-projectBlack-400">
                    A 3-layered structure, incorporating a Data Governance layer, an scalable Big Data Management and Processing layer, and an Applications Layer. In addition, it incorporates an open modular smart grid big data Analytics Toolbox and a Data / Models / Resources marketplace.
                </p>
            </p>
        </li>
        <li className="w-2/5">
            <p className="text-lg font-medium leading-6 text-projectBlack-400">
                How to get up to date with BD4NRG?
            </p>
            <p className="mt-2">
                <p className="text-base leading-6 text-projectBlack-400">
                    Just visit our <Link href="https://www.bd4nrg.eu/" target="_blank"  style={{color: 'blue'}}>official webpage</Link> and find out!
                </p>
            </p>
        </li>
    </ul>
</div>

}