import React from 'react'
import Link from 'next/link'

export default function Settings () {
  return (
    <>
      <div>
        <h1>BD4NRG Settings</h1>
      </div>
      <br />
      <h2>
        <Link href='/Settings/FAQ'>Check settings</Link>
      </h2>
      <br />
    </>
  )
}
