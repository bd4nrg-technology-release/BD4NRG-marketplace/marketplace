import '../styles/globals.css'
import Providers from './providers.js'
import SideBar from './common/sidebar/SideBar.js'
import NavBar from './common/navbar/NavBar.js'
import Footer from './common/footer/Footer.js'
import { Raleway } from '@next/font/google'

const raleway = Raleway({ weight: ['400'], subsets: ['latin'] })

export default function RootLayout ({ children }) {
  return (
    <html className={raleway.className}>
      <head/>
      <body>
          <NavBar/>
          <div className='p-8 sm:ml-64 mb-24 mt-16'>{children}</div>
          <Footer />
      </body>
    </html>
  )
}
