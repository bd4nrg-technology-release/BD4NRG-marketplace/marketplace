'use client'
import React, { useState, useEffect } from "react";
import './MLaaS.css';
import NotLoggedInPage from "../common/login/page.js";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

// This page is transforming into something too big, should be separated into components by service...
function MlaasTool() {
  const [augs_data, setAugsData] = useState("sample.csv");

  const [eais_data, setEaisData] = useState("X.csv");
  const [model, setModel] = useState('');
  const [modelSelect, setModelSelect] = useState('xgb.joblib')
  const [explainer, setExplainer] = useState('');
  const [explainerSelect, setExplainerSelect] = useState('shap.explainers._tree')
  const [models, setModels] = useState([]); // to store the models
  const [selectedModel, setSelectedModel] = useState(""); // to store the selected model
  const [buttonActive, setButtonActive] = useState(false); // to manage button active state

  const [ads_data, setAdsData] = useState("device_355_sample.csv");
  const [cycles, setCycles] = useState([]);
  const [responseCycle, setResponseCycle] = useState("");
  const [selectedCycle, setSelectedCycle] = useState("");
  const [token, setToken] = useState('');

  const [userDataADS, setUserDataADS] = useState([]);
  const [userDataAUGS, setUserDataAUGS] = useState([]);
  const [expandedCycle, setExpandedCycle] = useState(false)
  const toggleExpandedCycle = () => setExpandedCycle((current) => !current)
  const [expandedPlots, setExpandedPlots] = useState(false)
  const toggleExpandedPlots = () => setExpandedPlots((current) => !current)
  const [plotList, setPlotList] = useState([])

  // There is no env, lets use this at least
  // HTTPS server
  const rootUrlServices = 'https://api.marketplace.bd4nrg.eu'
  // Local deploy
  //const rootUrlServices = 'http://localhost:5080'

  const notifyError = () => toast.error("Error on sending the request.")
  const notifySuccess = () => toast.success('Success')

  useEffect(() => {
    // sessionStorage will now be accessed only on the client-side
    if (typeof window !== 'undefined') {
      const storedToken = sessionStorage.getItem('access_token');
      if (storedToken) {
        setToken(storedToken);
      }
    }
  }, []);

  useEffect(() => {
    fetchCycles();
    fetchModels();
    fetchUserData();
    fetchPlots();
  }, []);

  const fetchCycles = async () => {
    try {
      const auth_token = sessionStorage.getItem('access_token');
      console.log('RCJ fetchCycles, auth_token:', auth_token)
      // const response = await fetch('https://api.marketplace.bd4nrg.eu/bd4nrg/ads/cycles?auth_token=${authToken}');
      const response = await fetch(rootUrlServices + '/bd4nrg/ads/cycles?auth_token=' + auth_token);
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      const data = await response.json();
      setCycles(data.cycles);
    } catch (error) {
      console.error("Fetch error: ", error);
    }
  }

  const fetchModels = async () => {
    try {
      const auth_token = sessionStorage.getItem('access_token');
      console.log('RCJ fetchModels, auth_token:', auth_token)
      // const response = await fetch('https://api.marketplace.bd4nrg.eu/bd4nrg/eais/models?auth_token=${authToken}');
      // 
      const response = await fetch(rootUrlServices + '/bd4nrg/eais/models?auth_token=' + auth_token);
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      const models = await response.json();
      setModels(models);
    } catch (error) {
      console.error("Fetch error: ", error);
    }
  }

  // Gonna follow the same structure... even when all of this should be on a service...
  const fetchUserData = async () => {
    const auth_token = sessionStorage.getItem('access_token')
    const response = await fetch(rootUrlServices + '/das/data/user/list?auth_token=' + auth_token)
    const data = await response.json()
    // lets filter only cvs here
    const filteredCvs = data.filter(str => str.endsWith('.csv'))

    // const test = ['XXX_RGB.png', 'XXX_GTI.png', 'a', 'b']

    var filteredGTI = data.filter(str => str.includes('_GTI.'))
    filteredGTI = filteredGTI.map(x => x.replace(/_GTI.*/gm, ''))
    const filteredRGB = data.filter(str => str.includes('_RGB.'))
    const augsOptions = []
    filteredRGB.forEach(e => {
      const matcher = e.replace(/_RGB.*/gm, '')
      if (filteredGTI.includes(matcher)) {
        augsOptions.push(matcher)
      }
    });
    console.log(augsOptions)
    setUserDataAUGS(augsOptions)

    const formattedData = filteredCvs.map(filename => ({ filename }))
    setUserDataADS(formattedData)
  }

  // Gonna follow the same structure... even when all of this should be on a service...
  const fetchPlots = async () => {
    const auth_token = sessionStorage.getItem('access_token')
    const response = await fetch(rootUrlServices + '/das/data/user/plots?auth_token=' + auth_token)
    const data = await response.json()


    const formattedData = data.map(plots => ({ plots }))
    setPlotList(formattedData)
  }

  const handleModelSelect = (modelName) => {
    setSelectedModel(modelName);
    setButtonActive(true); // enable the button when a model is selected
  }

  const handleButtonClick = async () => {
    if (!buttonActive) {
      return; // if the button is not active, do nothing
    }

    // const response = await fetch(`https://api.marketplace.bd4nrg.eu/bd4nrg/eais/plots?model_name=${selectedModel}&auth_token=${authToken}`);
    const response = await fetch(rootUrlServices + '/bd4nrg/eais/plots?model_name=' + selectedModel + '&auth_token=' + auth_token);
    // handle the response as needed
    // ...
  }
  // Old version
  const handleSubmitEais = async (event) => {
    event.preventDefault();

    // const auth_token = localStorage.getItem('access_token');
    const auth_token = sessionStorage.getItem('access_token');
    console.log('RCJ handleSubmitAds, auth_token:', auth_token)
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ eais_data, model, explainer })
      // JSON.stringify({
      //   userId: 1,
      //   title: "Fix my bugs",
      //   completed: false
      // })
    };

    try {
      // const response = await fetch(`https://api.marketplace.bd4nrg.eu/bd4nrg/ads/detect?auth_token=${auth_token}`, requestOptions);
      const response = await fetch(rootUrlServices + '/bd4nrg/eais/explain?auth_token=' + auth_token, requestOptions);
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      const result = await response.text();
      console.log("Response: ", result);
    } catch (error) {
      console.error("Fetch error: ", error);
    }
  }
  // Table version
  const handleSubmitEaisTable = async (filename) => {

    // const auth_token = localStorage.getItem('access_token');
    const auth_token = sessionStorage.getItem('access_token');
    console.log('RCJ handleSubmitAds, auth_token:', auth_token)
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ 'filename': filename, 'model': modelSelect, 'explainer': explainerSelect })
      // JSON.stringify({
      //   userId: 1,
      //   title: "Fix my bugs",
      //   completed: false
      // })
    };
    console.log(JSON.stringify({ 'filename': filename, 'model': modelSelect, 'explainer': explainerSelect }))
    try {
      // const response = await fetch(`https://api.marketplace.bd4nrg.eu/bd4nrg/ads/detect?auth_token=${auth_token}`, requestOptions);
      const response = await fetch(rootUrlServices + '/bd4nrg/eais/explain?auth_token=' + auth_token, requestOptions);
      if (!response.ok) {
        notifyError()
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      const result = await response.json()
      console.log("Response: ", result)
      followUpExplain(result['job_id'], filename)
    } catch (error) {
      notifyError()
      console.error("Fetch error: ", error)
    }
  }

  // Follow up call to '/explain' call, 
  const followUpExplain = async (job_id, filename) => {
    const auth_token = sessionStorage.getItem('access_token')
    try {
      console.log("Starting followUpExplain");
      const response = await fetch(rootUrlServices + '/bd4nrg/eais/generate?job_id=' + job_id + '&filename=' + filename + '&plot_type=force_plot' + '&auth_token=' + auth_token)
      if (!response.ok) {
        notifyError()
        throw new Error(`HTTP error! status: ${response.status}`)
      }
      // There will be no response itslef, the plot is generating...
      const result = await response.text();
      notifySuccess()
      console.log("Response: ", result);
    } catch (error) {
      notifyError()
      console.error("Fetch error: ", error);
    }
  }
  // Select version
  const handleSubmitAds = async (event) => {
    event.preventDefault();

    // const auth_token = localStorage.getItem('access_token');
    const auth_token = sessionStorage.getItem('access_token');
    console.log('RCJ handleSubmitAds, auth_token:', auth_token)
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ ads_data })
    };

    try {
      // const response = await fetch(`https://api.marketplace.bd4nrg.eu/bd4nrg/ads/detect?auth_token=${auth_token}`, requestOptions);
      const response = await fetch(rootUrlServices + '/bd4nrg/ads/detect?auth_token=' + auth_token, requestOptions);
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      const result = await response.text();
      console.log("Response: ", result);
    } catch (error) {
      console.error("Fetch error: ", error);
    }
  }

  // Table version
  const handleSubmitAdsTable = async (fileName) => {
    const auth_token = sessionStorage.getItem('access_token')

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ fileName })
    }

    try {
      // const response = await fetch(`https://api.marketplace.bd4nrg.eu/bd4nrg/ads/detect?auth_token=${auth_token}`, requestOptions);
      const response = await fetch(rootUrlServices + '/bd4nrg/ads/detect?auth_token=' + auth_token, requestOptions);
      if (!response.ok) {
        notifyError()
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      const result = await response.text()
      console.log("Response: ", result)
      notifySuccess()
      // Should we something with this response? I mean its catched in case of errors, but why the log then?
      fetchCycles()
    } catch (error) {
      notifyError()
      console.error("Fetch error: ", error)
    }
  }

  // Old version
  const handleSubmitAugs = async (event) => {
    event.preventDefault();

    // const auth_token = localStorage.getItem('access_token');
    const auth_token = sessionStorage.getItem('access_token');
    console.log('RCJ handleSubmitAds, auth_token:', auth_token)
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ augs_data })
    };

    try {
      const response = await fetch(rootUrlServices + '/bd4nrg/augs/augment?auth_token=' + auth_token, requestOptions);
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      const result = await response.text();
      console.log("Response: ", result);
    } catch (error) {
      console.error("Fetch error: ", error);
    }
  }
  // Table version
  const handleSubmitAugsTable = async (fileName) => {
    const auth_token = sessionStorage.getItem('access_token');
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ fileName })
    };

    try {
      const response = await fetch(rootUrlServices + '/bd4nrg/augs/augment?auth_token=' + auth_token, requestOptions);
      if (!response.ok) {
        notifyError()
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      const result = await response.text();
      notifySuccess()
      console.log("Response: ", result);
    } catch (error) {
      notifyError()
      console.error("Fetch error: ", error);
    }
  }
  // Old version
  const handleCycleQuery = async () => {
    try {
      const encodedCycle = encodeURIComponent(selectedCycle);
      console.log('RCJ, encodedCycle:', encodedCycle)
      const auth_token = sessionStorage.getItem('access_token');
      console.log('RCJ handleCycleQuery, auth_token:', auth_token)
      // const response = await fetch(`https://api.marketplace.bd4nrg.eu/bd4nrg/ads/cycle?cycle=${encodedCycle}&auth_token=${authToken}`);
      // const response = await fetch(`http://localhost:5080/bd4nrg/ads/cycle?cycle=${encodedCycle}&auth_token=${auth_token}`);
      const response = await fetch(rootUrlServices + '/bd4nrg/ads/cycle?cycle=' + encodedCycle + '&auth_token=' + auth_token);
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      const result = await response.text();
      setResponseCycle(result)
      console.log("Response: ", result);
    } catch (error) {
      console.error("Fetch error: ", error);
    }
  }

  // Table version
  const handleCycleQueryTable = async (cycleName) => {
    try {
      const encodedCycle = encodeURIComponent(cycleName)
      console.log('RCJ, encodedCycle:', encodedCycle)
      const auth_token = sessionStorage.getItem('access_token')
      console.log('RCJ handleCycleQuery, auth_token:', auth_token)
      // const response = await fetch(`https://api.marketplace.bd4nrg.eu/bd4nrg/ads/cycle?cycle=${encodedCycle}&auth_token=${authToken}`);
      // const response = await fetch(`http://localhost:5080/bd4nrg/ads/cycle?cycle=${encodedCycle}&auth_token=${auth_token}`);
      const response = await fetch(rootUrlServices + '/bd4nrg/ads/cycle?cycle=' + encodedCycle + '&auth_token=' + auth_token)
      if (!response.ok) {
        notifyError()
        throw new Error(`HTTP error! status: ${response.status}`)
      }
      const result = await response.text()
      setResponseCycle(result)
      notifySuccess()
      console.log("Response: ", result)
    } catch (error) {
      notifyError()
      console.error("Fetch error: ", error)
    }
  }

  // Table version
  const handlePlotTable = async (cycleName) => {
    try {
      const encodedCycle = encodeURIComponent(cycleName)
      console.log('RCJ, encodedCycle:', encodedCycle)
      const auth_token = sessionStorage.getItem('access_token')
      console.log('RCJ handleCycleQuery, auth_token:', auth_token)
      // const response = await fetch(`https://api.marketplace.bd4nrg.eu/bd4nrg/ads/cycle?cycle=${encodedCycle}&auth_token=${authToken}`);
      // const response = await fetch(`http://localhost:5080/bd4nrg/ads/cycle?cycle=${encodedCycle}&auth_token=${auth_token}`);
      const response = await fetch(rootUrlServices + '/bd4nrg/ads/plot?cycle=' + encodedCycle + '&auth_token=' + auth_token)
      if (!response.ok) {
        notifyError()
        throw new Error(`HTTP error! status: ${response.status}`)
      }
      const result = await response.text()
      setResponseCycle(result)
      console.log("Response: ", result)
      notifySuccess()
    } catch (error) {
      console.error("Fetch error: ", error)
      notifyError()
    }
  }

  if (!token) {
    return <NotLoggedInPage />;
  }

  return (
    <div>
      <ToastContainer />
      <h2 className="text-4xl font-extrabold">Anomaly Detection Service</h2>
      <p className="mb-4 text-lg font-normal text-gray-400">
        This service is developed for use in the H2020 BD4NRG Research projects.
        The main goal of this service is to provide an easy-to-use and extendable service for ML based anomaly detection.
      </p>

      <div className="mb-4 p-6 bg-white border border-gray-200 shadow-md divide-y">
        <div className="mb-4 max-h-80 overflow-auto shadow-md">
          <table className="w-full text-sm text-left rtl:text-right text-gray-500">
            <thead className="top-0 sticky text-xs text-gray-700 uppercase bg-gray-50">
              <tr>
                <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                  File Name
                </th>
                <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              {userDataADS.map((dataset, index) => {
                return (
                  <tr key={index}>
                    <th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left text-blueGray-700 ">
                      {dataset.filename}
                    </th>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-1 ">
                      <button type="button" onClick={() => handleSubmitAdsTable(dataset.filename)} className="px-3 py-2 text-xs font-medium text-center inline-flex items-center focus:outline-none rounded-lg border hover:bg-gray-100 focus:z-10 focus:ring-4 focus:ring-gray-700 bg-gray-800 text-gray-400 border-gray-600 hover:text-white hover:bg-gray-700">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-6 h-6">
                          <path strokeLinecap="round" strokeLinejoin="round" d="M19.5 14.25v-2.625a3.375 3.375 0 0 0-3.375-3.375h-1.5A1.125 1.125 0 0 1 13.5 7.125v-1.5a3.375 3.375 0 0 0-3.375-3.375H8.25m6.75 12-3-3m0 0-3 3m3-3v6m-1.5-15H5.625c-.621 0-1.125.504-1.125 1.125v17.25c0 .621.504 1.125 1.125 1.125h12.75c.621 0 1.125-.504 1.125-1.125V11.25a9 9 0 0 0-9-9Z" />
                        </svg>
                        Submit
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>

        </div>

        <div className="mb-4 p-6 bg-white border border-gray-200 shadow-md divide-y">
          <button type="button" onClick={toggleExpandedCycle} className="w-full px-2 mb-2 text-2xl font-semibold tracking-tight text-gray-900 flex focus:outline-none hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-200">
            <p className="grow text-left">Cycles</p>

            {expandedCycle ?
              <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" className="w-8 h-8 right-0 ">
                <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m5 15 7-7 7 7" />
              </svg>
              :
              <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" className="w-8 h-8 right-0 ">
                <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m19 9-7 7-7-7" />
              </svg>
            }

          </button>
          <div className={`px-6 pt-0 overflow-hidden transition-[max-height] duration-500 ease-in ${expandedCycle ? "max-h-[500px]" : "max-h-0"}`}>
            <p className=" mt-3 mb-3 font-normal text-gray-500">Timeseries cycle detection and anomaly detection methods for the detection of anomalous cycles</p>
            <div className="mb-4 max-h-60 overflow-auto shadow-md">
              <table className="w-full text-sm text-left rtl:text-right text-gray-500">
                <thead className="top-0 sticky text-xs text-gray-700 uppercase bg-gray-50">
                  <tr>
                    <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                      Cycle Name
                    </th>
                    <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                      Action
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {cycles.map((cycle, index) => {
                    return (
                      <tr key={index}>
                        <th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left text-blueGray-700 ">
                          {cycle}
                        </th>
                        <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-1 ">
                          <button type="button" onClick={() => handleCycleQueryTable(cycle)} className="px-3 py-2 text-xs font-medium text-center inline-flex items-center focus:outline-none rounded-lg border hover:bg-gray-100 focus:z-10 focus:ring-4 focus:ring-gray-700 bg-gray-800 text-gray-400 border-gray-600 hover:text-white hover:bg-gray-700">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-6 h-6">
                              <path strokeLinecap="round" strokeLinejoin="round" d="M19.5 14.25v-2.625a3.375 3.375 0 0 0-3.375-3.375h-1.5A1.125 1.125 0 0 1 13.5 7.125v-1.5a3.375 3.375 0 0 0-3.375-3.375H8.25m6.75 12-3-3m0 0-3 3m3-3v6m-1.5-15H5.625c-.621 0-1.125.504-1.125 1.125v17.25c0 .621.504 1.125 1.125 1.125h12.75c.621 0 1.125-.504 1.125-1.125V11.25a9 9 0 0 0-9-9Z" />
                            </svg>
                            Submit
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
            <label className="block mb-2 text-sm font-medium text-gray-900">Cycle Response</label>
            <textarea id="message" rows="3" disabled={true} value={responseCycle} className="block p-2.5 w-full text-sm text-gray-900 bg-gray-200 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500"></textarea>
          </div>
        </div>
      </div>

      <h2 className="mt-8 text-4xl font-extrabold">Augmentation Service</h2>
      <p className="mb-4 text-lg font-normal text-gray-400">
        This service is developed for use in the H2020 BD4NRG Research projects.
        The main goal of this service is to provide an easy-to-use and extendable service for Geographical image
        augmentation.
      </p>

      <div className="mb-4 p-6 bg-white border border-gray-200 shadow-md divide-y">

        <div className="mb-4 max-h-80 overflow-auto shadow-md">
          <table className="w-full text-sm text-left rtl:text-right text-gray-500">
            <thead className="top-0 sticky text-xs text-gray-700 uppercase bg-gray-50">
              <tr>
                <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                  File Name
                </th>
                <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              {userDataAUGS.map((filename, index) => {
                return (
                  <tr key={index}>
                    <th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left text-blueGray-700 ">
                      {filename}
                    </th>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-1 ">
                      <button type="button" onClick={() => handleSubmitAugsTable(filename)} className="px-3 py-2 text-xs font-medium text-center inline-flex items-center focus:outline-none rounded-lg border hover:bg-gray-100 focus:z-10 focus:ring-4 focus:ring-gray-700 bg-gray-800 text-gray-400 border-gray-600 hover:text-white hover:bg-gray-700">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-6 h-6">
                          <path strokeLinecap="round" strokeLinejoin="round" d="M19.5 14.25v-2.625a3.375 3.375 0 0 0-3.375-3.375h-1.5A1.125 1.125 0 0 1 13.5 7.125v-1.5a3.375 3.375 0 0 0-3.375-3.375H8.25m6.75 12-3-3m0 0-3 3m3-3v6m-1.5-15H5.625c-.621 0-1.125.504-1.125 1.125v17.25c0 .621.504 1.125 1.125 1.125h12.75c.621 0 1.125-.504 1.125-1.125V11.25a9 9 0 0 0-9-9Z" />
                        </svg>
                        Submit
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>

        </div>
      </div>

      <h2 className="mt-8 text-4xl font-extrabold">Explainable AI</h2>
      <p className="mb-4 text-lg font-normal text-gray-400">
        This service is developed for use in the H2020 BD4NRG Research projects.
        Its main goals is to provide a service capable of offering additional information regarding
        ML based predictive model inferences and performance.
      </p>

      <div className="mb-4 p-6 bg-white border border-gray-200 shadow-md divide-y">

        <label for="model" className="block mb-2 text-sm font-medium">Choose a Model</label>
        <select id="model" onChange={(e) => { setModelSelect(e.target.value) }} className="bg-gray-50 mb-4 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
          <option value='xgb.joblib'>
            xgb.joblib
          </option>

        </select>

        <label for="model" className="block mb-2 text-sm font-medium">Choose an Explainer</label>
        <select id="model" onChange={(e) => { setExplainerSelect(e.target.value) }} className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
          <option value='xgb.joblib'>
            shap.explainers._tree
          </option>
        </select>

        <div className="mb-4 mt-6 max-h-80 overflow-auto shadow-md">
          <table className="w-full text-sm text-left rtl:text-right text-gray-500">
            <thead className="top-0 sticky text-xs text-gray-700 uppercase bg-gray-50">
              <tr>
                <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                  File Name
                </th>
                <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              {userDataADS.map((dataset, index) => {
                return (
                  <tr key={index}>
                    <th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left text-blueGray-700 ">
                      {dataset.filename}
                    </th>
                    <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-1 ">
                      <button type="button" onClick={() => handleSubmitEaisTable(dataset.filename)} className="px-3 py-2 text-xs font-medium text-center inline-flex items-center focus:outline-none rounded-lg border hover:bg-gray-100 focus:z-10 focus:ring-4 focus:ring-gray-700 bg-gray-800 text-gray-400 border-gray-600 hover:text-white hover:bg-gray-700">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-6 h-6">
                          <path strokeLinecap="round" strokeLinejoin="round" d="M19.5 14.25v-2.625a3.375 3.375 0 0 0-3.375-3.375h-1.5A1.125 1.125 0 0 1 13.5 7.125v-1.5a3.375 3.375 0 0 0-3.375-3.375H8.25m6.75 12-3-3m0 0-3 3m3-3v6m-1.5-15H5.625c-.621 0-1.125.504-1.125 1.125v17.25c0 .621.504 1.125 1.125 1.125h12.75c.621 0 1.125-.504 1.125-1.125V11.25a9 9 0 0 0-9-9Z" />
                        </svg>
                        Submit
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>

        <div className="mb-4 p-6 bg-white border border-gray-200 shadow-md divide-y">
          <button type="button" onClick={toggleExpandedPlots} className="w-full px-2 mb-2 text-2xl font-semibold tracking-tight text-gray-900 flex focus:outline-none hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-200">
            <p className="grow text-left">Plots</p>

            {expandedPlots ?
              <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" className="w-8 h-8 right-0 ">
                <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m5 15 7-7 7 7" />
              </svg>
              :
              <svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" className="w-8 h-8 right-0 ">
                <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m19 9-7 7-7-7" />
              </svg>
            }

          </button>
          <div className={`px-6 pt-0 overflow-hidden transition-[max-height] duration-500 ease-in ${expandedPlots ? "max-h-[500px]" : "max-h-0"}`}>
            {/* <p className=" mt-3 mb-3 font-normal text-gray-500">Whatever we want to say about plots, like they only appear when the job ends or something</p> */}
            <div className="mb-4 max-h-60 overflow-auto shadow-md">
              <table className="w-full text-sm text-left rtl:text-right text-gray-500">
                <thead className="top-0 sticky text-xs text-gray-700 uppercase bg-gray-50">
                  <tr>
                    <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                      Plot Name
                    </th>
                    <th className="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                      Action
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {plotList.map((plot, index) => {
                    return (
                      <tr key={index}>
                        <th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left text-blueGray-700 ">
                          {plot}
                        </th>
                        <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-1 ">
                          <button type="button" onClick={() => handlePlotTable(plot)} className="px-3 py-2 text-xs font-medium text-center inline-flex items-center focus:outline-none rounded-lg border hover:bg-gray-100 focus:z-10 focus:ring-4 focus:ring-gray-700 bg-gray-800 text-gray-400 border-gray-600 hover:text-white hover:bg-gray-700">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-6 h-6">
                              <path strokeLinecap="round" strokeLinejoin="round" d="M19.5 14.25v-2.625a3.375 3.375 0 0 0-3.375-3.375h-1.5A1.125 1.125 0 0 1 13.5 7.125v-1.5a3.375 3.375 0 0 0-3.375-3.375H8.25m6.75 12-3-3m0 0-3 3m3-3v6m-1.5-15H5.625c-.621 0-1.125.504-1.125 1.125v17.25c0 .621.504 1.125 1.125 1.125h12.75c.621 0 1.125-.504 1.125-1.125V11.25a9 9 0 0 0-9-9Z" />
                            </svg>
                            Download
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      {/* <div className="tool-container">
        <h1><th className="model-table th">Anomaly Detection Service</th></h1>
        <p>This service is developed for use in the H2020 BD4NRG Research projects.
          The main goal of this service is to provide an easy-to-use and extendable service for ML based anomaly detection.
        </p>
        <form onSubmit={handleSubmitAds}>
          <label>
            Data:
            <input type="text" value={ads_data} onChange={e => setAdsData(e.target.value)} />
          </label>
          <button className="tool-submit-button" type="submit">Submit</button>
        </form>
        <div className="cycles-list">
          <h2>Select a Cycle</h2>
          <ul>
            {cycles.map((cycle, index) => (
              <li key={index} onClick={() => setSelectedCycle(cycle)} className={selectedCycle === cycle ? "selected-cycle" : ""}>
                {cycle}
              </li>
            ))}
          </ul>
        </div>
        <button className="tool-submit-button" onClick={handleCycleQuery} disabled={!selectedCycle}>
          Get Cycle Data
        </button>
      </div>

      <div className="tool-container">
        <h1><th className="model-table th">Augmentation Service</th></h1>
        <p>This service is developed for use in the H2020 BD4NRG Research projects.
          The main goal of this service is to provide an easy-to-use and extendable service for Geographical image
          augmentation.
        </p>
        <form onSubmit={handleSubmitAugs}>
          <label>
            Mock data
            <input type="text" value={augs_data} onChange={e => setAugsData(e.target.value)} />
          </label>
          <button className="tool-submit-button" type="submit">Submit</button>
        </form>
      </div>

      <div className="tool-container">
        <h1><th className="model-table th">Explainable AI</th></h1>
        <p>This service is developed for use in the H2020 BD4NRG Research projects.
          Its main goals is to provide a service capable of offering additional information regarding
          ML based predictive model inferences and performance.
        </p>
        <br></br>
        <form onSubmit={handleSubmitEais}>
          <label>
            Data:
            <input type="text" value={eais_data} onChange={e => setEaisData(e.target.value)} />
          </label>
          <label>
            Model (select):
            <input type="text" value={model} onChange={e => setModel(e.target.value)} />
          </label>
          <label>
            Explainer ():
            <input type="text" value={explainer} onChange={e => setExplainer(e.target.value)} />
          </label>
          <button className="tool-submit-button" type="submit">Submit</button>
        </form>

        <h2><th className="model-table th">Choose model to obtain results</th></h2>
        <table>
          <tbody>
            {models.map((modelName, index) => (
              <tr key={index} onClick={() => handleModelSelect(modelName)} className={selectedModel === modelName ? "selected" : ""}>
                <td>{modelName}</td>
              </tr>
            ))}
          </tbody>
        </table>

        <button className="tool-submit-button" onClick={handleButtonClick} disabled={!buttonActive}>
          Get Plots
        </button>
      </div> */}
    </div>
  );
};

export default MlaasTool;
