'use client'
import { signIn } from 'next-auth/react'
export default function SignInButton () {
  return (
    <button onClick={() => signIn('keycloak')} className="text-white">Sign in with Keycloak</button>
  )
}
