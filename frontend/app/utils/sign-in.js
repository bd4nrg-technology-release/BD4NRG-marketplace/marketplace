export function isUserLoggedIn (session) {
  return Boolean(session)
}

export function hasUserRightRoles (userRoles, accessRole) {
  return userRoles.indexOf(accessRole) !== -1 || isAdmin(userRoles)
}

export function isAdmin (roles) {
  return roles.indexOf('admin') !== -1
}
