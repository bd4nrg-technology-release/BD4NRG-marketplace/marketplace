export default function Head () {
  return (
    <>
      <title> BD4NRG Marketplace Dashboard </title>
      <meta content='width=device-width, initial-scale=1' name='viewport' />
      <link rel='icon' href='/img/bd4nrg-logo.png' sizes='32x32' />

    </>
  )
}
