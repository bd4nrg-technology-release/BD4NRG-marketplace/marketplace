export default function Select () {

return <select className="block px-3 py-2 text-gray-700 bg-white border border-gray-300 rounded-md shadow-sm w-52 focus:outline-none focus:ring-primary-500 focus:border-primary-500" name="animals">
    <option value="">
        Check available services
    </option>
    <option value="pvprod">
        Photovoltaic production
    </option>
    <option value="loadforecast">
        Load forecasting
    </option>
    <option value="waterpum">
        Water pumping
    </option>
    <option value="prediction">
        Prediction services
    </option>
</select>

}
