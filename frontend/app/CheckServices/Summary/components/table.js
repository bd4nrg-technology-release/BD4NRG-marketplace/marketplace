export default function Table () {

    return <table className="table p-4 bg-white rounded-lg shadow">
        <thead>
            <tr>
                <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
                    Service Name
                </th>
                <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
                    Type
                </th>
                <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
                    Description
                </th>
                <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
                    Application
                </th>
                <th className="border-b-2 p-4 dark:border-dark-5 whitespace-nowrap font-normal text-gray-900">
                    Other
                </th>
            </tr>
        </thead>
        <tbody>
            <tr className="text-gray-700">
                <td className="border-b-2 p-4 dark:border-dark-5">
                    Photovoltaic production
                </td>
                <td className="border-b-2 p-4 dark:border-dark-5">
                    TBD
                </td>
                <td className="border-b-2 p-4 dark:border-dark-5">
                    TBD
                </td>
                <td className="border-b-2 p-4 dark:border-dark-5">
                    TBD
                </td>
                <td className="border-b-2 p-4 dark:border-dark-5">
                    TBD
                </td>
            </tr>
            <tr className="text-gray-700">
                <td className="border-b-2 p-4 dark:border-dark-5">
                    Load forecasting
                </td>
                <td className="border-b-2 p-4 dark:border-dark-5">
                    TBD
                </td>
                <td className="border-b-2 p-4 dark:border-dark-5">
                    TBD
                </td>
                <td className="border-b-2 p-4 dark:border-dark-5">
                    TBD
                </td>
                <td className="border-b-2 p-4 dark:border-dark-5">
                    TBD
                </td>
            </tr>    
        </tbody>
    </table>
    
    }