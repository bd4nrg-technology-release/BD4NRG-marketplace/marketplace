import React from 'react'

export default function About () {
  return (
    <div>
      <h1>About Page</h1>
    </div>
  )
}

;<button
  id='dropdownDividerButton'
  data-dropdown-toggle='dropdownDivider'
  className='inline-flex items-center py-2.5 px-4 text-sm font-medium text-center text-white bg-blue-700 rounded-lg dark:bg-blue-600 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 focus:outline-none dark:hover:bg-blue-700 dark:focus:ring-blue-800'
  type='button'
>
  Dropdown divider{' '}
  <svg
    className='ml-2 w-4 h-4'
    aria-hidden='true'
    fill='none'
    stroke='currentColor'
    viewBox='0 0 24 24'
    xmlns='http://www.w3.org/2000/svg'
  >
    <path
      stroke-linecap='round'
      stroke-linejoin='round'
      stroke-width='2'
      d='M19 9l-7 7-7-7'
    ></path>
  </svg>
</button>
