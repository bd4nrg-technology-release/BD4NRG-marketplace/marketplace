'use client'
import './Assets.css';
import React, { useEffect, useState } from "react";
import NotLoggedInPage from "../common/login/page.js";
import { Button, Modal, TextInput, Label } from 'flowbite-react';

const assets = [
  { title: "Load Forecasting Island", description: "Lorem ipsum", dockerurl: "registry.bd4nrg.ari-energy.eu:80/bd4_load_forecasting_island", protobuf: "load_forecasting_island-1.0.0.proto" },
  { title: "Photovoltaic Production Forecasting", description: "Lorem ipsum", dockerurl: "registry.bd4nrg.ari-energy.eu:80/bd4_photovoltaic", protobuf: "photovoltaic_production_forecasting-1.0.0.proto" },
  { title: "Restricted plans", description: "Not available", dockerurl: "registry.bd4nrg.ari-energy.eu:80/bd4_restricted_plans", protobuf: "restricted_plans-1.0.0.proto" },
  { title: "Derisking predictions", description: "Lorem ipsum", dockerurl: "registry.bd4nrg.ari-energy.eu:80/bd4_derisking_predictions", protobuf: "derisking_predictions-1.0.0.proto" },
  { title: "Power scheduling", description: "Not available", dockerurl: "registry.bd4nrg.ari-energy.eu:80/bd4_power_scheduling", protobuf: "power_scheduling-1.0.0.proto" },
  { title: "Unrestricted plans", description: "Lorem ipsum", dockerurl: "registry.bd4nrg.ari-energy.eu:80/bd4_unrestricted_plans", protobuf: "unrestricted_plans-1.0.0.proto" },
  { title: "IPMVP-based Energy Efficiency Renovations Assessment", description: "Not available", dockerurl: "registry.bd4nrg.ari-energy.eu:80/bd4_ipmvp", protobuf: "ipmvp-based_energy_efficiency_renovations_assessment-1.0.0.proto" },
  { title: "LSP3-UC2-GUI", description: "Lorem ipsum", dockerurl: "registry.bd4nrg.ari-energy.eu:80/analitica-downloader:4.0.4", protobuf: "lsp3-gui-1.0.0.proto" },
  { title: "LSP3-Aging-Prediction", description: "Lorem ipsum", dockerurl: "registry.bd4nrg.ari-energy.eu:80/analitica-downloader:1.0.3", protobuf: "lsp3-aging-prediction-1.0.0.proto" },
  { title: "LSP3-Power-Prediction", description: "Lorem ipsum", dockerurl: "registry.bd4nrg.ari-energy.eu:80/onboarding-mlmodel:3.0.0", protobuf: "lsp3-power-prediction-1.0.0.proto" },
  { title: "ads service", description: "Not available", dockerurl: "registry.bd4nrg.ari-energy.eu:80/bd4nrg_ads_analytics", protobuf: "ads_service-1.0.0.proto" },
  { title: "eais service", description: "Lorem ipsum", dockerurl: "registry.bd4nrg.ari-energy.eu:80/bd4nrg_eais_analytics", protobuf: "eais_service-1.0.0.proto" },
];

//{ title: "lsp1-fault-detection", description:"Not available", dockerurl:"025760424976.dkr.ecr.eu-west-1.amazonaws.com/bd4nrg-fault-detection:0.0.1", protobuf:"lsp1-fault-detection-1.0.0.proto" }, 
//{ title: "lsp1-fault-classification", description:"Not available", dockerurl:"025760424976.dkr.ecr.eu-west-1.amazonaws.com/bd4nrg-fault-classification:0.0.1", protobuf:"lsp1-fault-classification-1.0.0.proto" }, 
//{ title: "lsp1-incident-report", description:"Not available", dockerurl:"025760424976.dkr.ecr.eu-west-1.amazonaws.com/bd4nrg-incident-report:0.0.1", protobuf:""lsp1-incident-report-1.0.0.proto }, 
//{ title: "lsp1-databroker", description:"Not available", dockerurl:"025760424976.dkr.ecr.eu-west-1.amazonaws.com/bd4nrg-databroker:0.0.1", protobuf:"lsp1-databroker-1.0.0.proto"} 

function AssetsPage() {
  const [selectedAsset, setSelectedAsset] = useState(null);
  const [modalName, onChangeModalName] = useState('');
  const [token, setToken] = useState('');
  const [openModal, setOpenModal] = useState(false);

  useEffect(() => {
    // sessionStorage will now be accessed only on the client-side
    if (typeof window !== 'undefined') {
      const storedToken = sessionStorage.getItem('access_token');
      if (storedToken) {
        setToken(storedToken);
      }
    }
  }, []);

  const handleAssetClick = (asset) => {
    setSelectedAsset(asset);
  };

  const handleOnBoard = async () => {
    if (!selectedAsset) return;

    // // Prompt the user for the onboarded model's name
    // const modelName = window.prompt("Enter the onboarded model's name:", "");

    // If the user cancels or doesn't provide a name, don't proceed
    if (modalName === null || modalName.trim() === "") {
      alert("You must provide a name to proceed.");
      return;
    }

    const auth_token = sessionStorage.getItem('access_token');
    const onboardURL = new URL('https://api.marketplace.bd4nrg.eu/analytics/onboard');
    onboardURL.search = new URLSearchParams({
      auth_token: auth_token,
    });
    const response = await fetch(onboardURL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        asset: selectedAsset.dockerurl,
        proto: selectedAsset.protobuf,
        modelName: modalName  // Send the onboarded model's name in the request body
      }),
    });
    console.log(response)
  };

  if (!token) {
    return <NotLoggedInPage />;
  }

  function handleModal(asset) {
    setSelectedAsset(asset)
    setOpenModal(true)
  }

  return (
    <div>
      <h2 className="text-4xl font-extrabold">Analytics Toolbox Service</h2>
      <p className="mb-4 text-lg font-normal text-gray-400">
        In order to execute any of the services available you must first have registered into the Analytics Toolbox (https://analyticstoolbox.bd4nrg.eu/) with the same user and email as you have in BD4NRG.
      </p>
      {/* <h2 className="assets-header">Onboarding Service</h2> */}
      {/* <p> To onboard your service in your Analytics Toolbox a series of steps must be followed. Once you have decided which Service you want:
      </p> */}
      {/* <ol>
        <li> 1. Download the protobuf file</li>
        <li> 2. Copy the Docker images URL</li>
      </ol>  */}
      {/* <p> Select the model you would like to onboard in the Analytics toolbox service</p> */}

      <ul role="list" className="grid grid-cols-1 gap-6 sm:grid-cols-2 lg:grid-cols-3">
        {assets.map((asset, index) => (
          <li key={index} className="col-span-1 divide-y divide-gray-200 rounded-lg bg-white shadow-lg">
            <div className="flex w-full items-center justify-between space-x-6 p-6">
              <div className="flex-1">
                <div className="flex items-center space-x-3">
                  <p className="font-bold text-sm text-gray-900">{asset.title}</p>
                  <span className="inline-flex flex-shrink-0 items-center rounded-full bg-green-50 px-1.5 py-0.5 text-xs font-medium text-blue-600 ring-1 ring-inset ring-green-600/20">Model</span>
                </div>
                {/* <p className="mt-1 text-sm text-gray-500">{asset.description}</p> */}
              </div>
            </div>
            <div>
              <div className="-mt-px flex divide-x bottom-0 divide-gray-200">
                <div className="flex w-0 flex-1">
                  <button onClick={() => { handleModal(asset) }} className="relative -mr-px inline-flex w-0 flex-1 items-center justify-center gap-x-3 rounded-bl-lg border border-transparent py-4 text-sm font-semibold text-gray-900 hover:bg-gray-900 hover:text-white transition-colors duration-300 ease-in-out">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-6 h-6">
                      <path strokeLinecap="round" strokeLinejoin="round" d="M12 4.5v15m7.5-7.5h-15" />
                    </svg>
                    Select Model
                  </button>
                </div>
              </div>
            </div>
          </li>
        ))}
      </ul>

      <Modal show={openModal} onClose={() => setOpenModal(false)}>
        <Modal.Header>
          <div className="flex flex-row items-center">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-6 h-6">
              <path strokeLinecap="round" strokeLinejoin="round" d="M12 16.5V9.75m0 0 3 3m-3-3-3 3M6.75 19.5a4.5 4.5 0 0 1-1.41-8.775 5.25 5.25 0 0 1 10.233-2.33 3 3 0 0 1 3.758 3.848A3.752 3.752 0 0 1 18 19.5H6.75Z" />
            </svg>
            {selectedAsset && <p className="ml-2">{selectedAsset.title}</p>}
          </div>
        </Modal.Header>
        <Modal.Body>
          <div className="space-y-6">
            <div>
              <div className="mb-2 block">
                <Label htmlFor="small" value="Enter the onboarded model's name:" />
              </div>
              <TextInput type="text" sizing="sm" onChange={(event) => onChangeModalName(event.target.value)} value={modalName} />
            </div>

          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => handleOnBoard()}>
            Go
          </Button>
          <Button color="gray" onClick={() => setOpenModal(false)}>
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>


    </div>
  );
}


export default AssetsPage;
