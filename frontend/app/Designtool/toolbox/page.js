import React from "react";
import Form from './components/form.js';
import DDD from './components/ddd.js';
import DDDdata from './components/ddddata.js';
  
export function AnalyticsToolbox () {
  return (
    <><><><div>
      <font size='30' align='right'>BD4NRG Analytics Toolbox presentation</font>
    </div><br /><h1>Create your own BD4NRG-based application</h1></>
    <div align="right"><DDD /></div><div align="center"><DDDdata /></div><br />
    <br /><br /><br /><br /><br />
    <></><Form /></>
    <h1>Combine data sources of your choice with BD4NRG algorithms and get interesting results</h1><br /></>
  );
};
  


export default AnalyticsToolbox;