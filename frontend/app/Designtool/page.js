import React from "react";
import Feature from './components/feature.js';
import Link from 'next/link';
  
export function DesignTool () {
  return (
    <><div>
      {/* <h1>BD4NRG <Link href="/Designtool/toolbox">Design Tool helps you to combine data and services</Link></h1> */}
      <h1>BD4NRG <Link href="https://aiexp-preprod.ai4europe.eu/#/marketPlace#marketplaceTemplate" target="_blank" style={{color: 'blue'}}>Design Tool </Link>helps you to combine data and services</h1>
    </div>
    <br /><Feature /></>
  );
};
  
<div style={{
      position: 'absolute', left: '50%', top: '70%',
      transform: 'translate(-50%, -50%)'
    }}></div>

export default DesignTool;
