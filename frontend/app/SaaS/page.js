'use client'
import React, { useState, useEffect } from "react";
import "./saas.css";
import NotLoggedInPage from "../common/login/page.js";

function SaaS() {
  const [token, setToken] = useState('');

  useEffect(() => {
    // sessionStorage will now be accessed only on the client-side
    if (typeof window !== 'undefined') {
      const storedToken = sessionStorage.getItem('access_token');
      if (storedToken) {
        setToken(storedToken);
      }
    }
  }, []);

  if (!token) {
    return <NotLoggedInPage />;
  }

  return (
    <div>
      <h2 className="text-4xl font-extrabold">Simulation as a Service</h2>
      <p className="mb-4 text-lg font-normal text-gray-400">
        BD4NRG Analytics Toolbox
      </p>
      <iframe src="https://villas.k8s.eonerc.rwth-aachen.de" title="SaaS" className="w-full aspect-video"></iframe>
    </div>
  );
}

export default SaaS;
